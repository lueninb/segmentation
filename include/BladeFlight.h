#include <iostream>
#include <tuple>
#include <vector>
#include <math.h>

using namespace std;

class BladeFlight{

private:
	vector< tuple<double, double, double> > leadingEdge;
	double angle;
public:
	BladeFlight(double _angle, const vector < tuple<double, double, double> > & _leadingEdge):angle{_angle}, leadingEdge{_leadingEdge}{};
	double getAngle();
	tuple<double, double, double> getHome();
	double getHomeSleep();
};

tuple<double, double, double> BladeFlight::getHome()
{
	double x = 0;
	double y = 0;
	double z = 0;
	for(int i =0; i < leadingEdge.size(); i++)
	{
		x += - get<0>(leadingEdge[i]);
		y += - get<1>(leadingEdge[i]);
		z += - get<2>(leadingEdge[i]);
	}
	return make_tuple(x, y, z);	
}

double BladeFlight::getHomeSleep()
{
	double distanceTraveled = 0;
	
	for(int i =0; i < leadingEdge.size(); i++)
	{
		double x = get<0>(leadingEdge[i]);
		double y = get<1>(leadingEdge[i]);
		double z = get<2>(leadingEdge[i]);
		
		distanceTraveled += sqrt(x*x + y*y + z*z);
	}
	return distanceTraveled * 1.0;	
}
