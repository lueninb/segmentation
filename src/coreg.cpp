#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>

#include <vector>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/common/common.h>
#include <pcl/filters/extract_indices.h>
#include <math.h>

/* Livox Lidar Coordinate System
 * +x - forward
 * +y - left
 * +z - up
*/

ros::Publisher pubTruth;
ros::Publisher pubIncomingCloud;
ros::Publisher pubICPresults;
sensor_msgs::PointCloud2 output_groundTruth;

#define PI 3.14159265
#define VIEWER_ON 0 

pcl::PointCloud<pcl::PointXYZ>::Ptr cloudOut(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr groundTruthCloud(new pcl::PointCloud<pcl::PointXYZ>);
int cloudIndex = 0;

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
	// Container for original & filtered data
	pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2; 
	pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
	pcl::PCLPointCloud2 cloud_filtered;

	// Convert to PCL data type
	pcl_conversions::toPCL(*cloud_msg, *cloud);

	// Perform the actual filtering
	pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
	sor.setInputCloud (cloudPtr);
	sor.setLeafSize (0.012, 0.012, 0.012);
	sor.filter (cloud_filtered);

	pcl::PointCloud<pcl::PointXYZ> point_cloud;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudIn (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromPCLPointCloud2( cloud_filtered, point_cloud);

	//Original cloud is ground truth
	if (cloudIndex == 0)
		pcl::copyPointCloud(point_cloud, *cloudOut);
	else
		pcl::copyPointCloud(point_cloud, *cloudIn);
		//Incoming clouds
		
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	
	if (cloudIndex == 0)
		tree->setInputCloud(cloudOut);
	else
		tree->setInputCloud(cloudIn);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance(0.065); // 2cm
	ec.setMinClusterSize(250); //100
	ec.setMaxClusterSize(99000000);
	ec.setSearchMethod(tree);
	if (cloudIndex == 0)
		ec.setInputCloud(cloudOut);
	else
		ec.setInputCloud(cloudIn);
	ec.extract(cluster_indices);
	
	//Calculate the centroid of each cluster
	pcl::PointCloud<pcl::PointXYZ>::Ptr msg (new pcl::PointCloud<pcl::PointXYZ>);
	
	float xTower, yTower, zTower = FLT_MAX;
	float hubDistance = FLT_MAX;
  	int towerIndex = 0;
	int hubIndex = 0;
	for (size_t i = 0; i < cluster_indices.size(); i++)
	{
		Eigen::Vector4f centroid;
		if (cloudIndex == 0)
			pcl::compute3DCentroid(*cloudOut, cluster_indices[i], centroid);
		else
			pcl::compute3DCentroid(*cloudIn, cluster_indices[i], centroid);
		
		//Find index of hub and tower centroids. We use this index to keep only hub & tower clusters
		if (centroid[2] < zTower && centroid[2] < 0)
		{
			zTower = centroid[2];
			towerIndex = i;
		}
		
		if (sqrt(centroid[0] * centroid[0] + centroid[1] * centroid[1] + centroid[2] * centroid[2]) < hubDistance)
		{
			hubDistance = sqrt(centroid[0] * centroid[0] + centroid[1] * centroid[1] + centroid[2] * centroid[2]);
			hubIndex = i;
		}
	}
  	//The hub and tower indices
	std::cout << "Hub idx " << hubIndex << " " << "Tower idx " << towerIndex << std::endl;

	//Remove from the clustered cloud all clusters except hub and tower
	pcl::PointIndices::Ptr removePoints(new  pcl::PointIndices(cluster_indices[towerIndex]));
	pcl::PointIndices::Ptr removePoints2(new  pcl::PointIndices(cluster_indices[hubIndex]));
	pcl::PointCloud<pcl::PointXYZ>::Ptr testCloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr testCloud2(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::ExtractIndices<pcl::PointXYZ> extract;
	
	if (cloudIndex == 0)
		extract.setInputCloud (cloudOut);
	else
		extract.setInputCloud (cloudIn);
	
	extract.setIndices (removePoints);
	extract.filter (*testCloud);
	
	if (cloudIndex == 0)
		extract.setInputCloud (cloudOut);
	else
		extract.setInputCloud (cloudIn);
	
	extract.setIndices (removePoints2);
	extract.filter (*testCloud2);
	
	*testCloud += *testCloud2;
	if (cloudIndex == 0)
		*groundTruthCloud = *testCloud;
		
	// Convert to ROS data type
	testCloud->header.frame_id = cloudOut->header.frame_id;
	if(testCloud->size()) pcl::toPCLPointCloud2(*testCloud, cloud_filtered);
	else pcl::toPCLPointCloud2(*cloudOut, cloud_filtered);
	sensor_msgs::PointCloud2 output;
	
	if (cloudIndex == 0)
		pcl_conversions::fromPCL(cloud_filtered, output_groundTruth);	
	
	pcl_conversions::fromPCL(cloud_filtered, output);	
	
	// Publish the clustered/segmented ground truth cloud and incoming data
	pubTruth.publish (output_groundTruth);
	pubIncomingCloud.publish(output);	
		
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr finalCloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudOut_new (new pcl::PointCloud<pcl::PointXYZ>);
	// After creating ground truth cloud, perform icp against incoming clouds
	if (cloudIndex > 0)
	{
		pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
		icp.setInputSource(groundTruthCloud);
		icp.setInputTarget(testCloud);
		icp.setMaximumIterations (500);
		icp.setTransformationEpsilon (1e-9);
		icp.setMaxCorrespondenceDistance (250);
		icp.setEuclideanFitnessEpsilon (1);
		icp.setRANSACOutlierRejectionThreshold (2.5);
		
		icp.align(*cloudOut_new);
		
		if (icp.hasConverged())
		{
			std::cout << "ICP converged." << std::endl
				<< "The score is " << icp.getFitnessScore() << std::endl;
			std::cout << "Transformation matrix:" << std::endl;
			std::cout << icp.getFinalTransformation() << std::endl;
		}
		else 
			std::cout << "ICP did not converge." << std::endl;
			
		*finalCloud=*testCloud;
		*finalCloud+=*cloudOut_new;
		
		sensor_msgs::PointCloud2 icpResults;
		pcl::PCLPointCloud2 icpCloud;
		pcl::toPCLPointCloud2(*testCloud, icpCloud);
		pcl_conversions::fromPCL(icpCloud, icpResults);	
		pubICPresults.publish(icpResults);
		
		//Viewer for pcl icp results
		if (VIEWER_ON)
		{
			boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer("3D Viewer"));
			viewer->setBackgroundColor(0,0,0);
			pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color1 (testCloud, 0, 0, 200);
			pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color2 (cloudOut_new, 200, 0, 0);

			viewer->addPointCloud<pcl::PointXYZ> (testCloud,single_color1, "sample_cloud_1");
			viewer->addPointCloud<pcl::PointXYZ> (cloudOut_new, single_color2, "sample_cloud_2");

			viewer->addCoordinateSystem(1.0);
			viewer->initCameraParameters();
			pcl::visualization::Camera* temp = new pcl::visualization::Camera();
			
			//Camera parameters found through mouse
			temp->focal[0] = 9.28449;
			temp->focal[1] = -2.85358;
			temp->focal[2] = -2.10611;
			
			temp->pos[0] = -7.30781;
			temp->pos[1] = -3.2626;
			temp->pos[2] = -2.77215;
			
			temp->view[0] = -0.0385167;
			temp->view[1] = -0.0616078;
			temp->view[2] = 0.997357;
			
			temp->clip[0] =  0.0553202;
			temp->clip[1] = 55.3202;
			
			temp->fovy = 49.1311 * PI/180;
			
			temp->window_size[0] = 960;
			temp->window_size[1] = 540;
			
			temp->window_pos[0] = 65;
			temp->window_pos[1] = 52;
			
			/*
			clip 0.0553202, 55.3202
			Position [x,y,z] -7.30781, -3.2626, -2.77215
			View up [x,y,z] -0.0385167, -0.0616078, 0.997357
			Camera view angle [degrees] 49.1311
			Window size [x,y] 960, 540
			Window position [x,y] 65, 52
			*/
			
			viewer->setCameraParameters(*temp, 0);
			//while(!viewer->wasStopped())
			//{
			  viewer->spinOnce();
			  boost::this_thread::sleep (boost::posix_time::microseconds(100000));

			//}
		}
	}
	//ros::spinOnce();
	cloudIndex++;
}

int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "turbine_segmentation");
	ros::NodeHandle nh;

	// Create a ROS subscriber for the input point cloud
	ros::Subscriber sub = nh.subscribe ("/livox/lidar", 1, cloud_cb);
	pubTruth = nh.advertise<sensor_msgs::PointCloud2> ("groundTruth_cloud", 1);
	pubIncomingCloud = nh.advertise<sensor_msgs::PointCloud2> ("incoming_cloud", 1);
	pubICPresults = nh.advertise<sensor_msgs::PointCloud2> ("icpResult", 1);
	

	
	ros::spin();

}
