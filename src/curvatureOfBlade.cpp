#include <ros/ros.h>

#include <math.h>
#include <vector>
#include <numeric>
#include <tuple>

#include "matplotlibcpp.h"
#include <turbine_segmentation/BladePath.h>
#include <turbine_segmentation/Waypoints.h>
#include <turbine_segmentation/WaypointsPath.h>
#include <sensor_msgs/NavSatFix.h>
#include "geometry_msgs/QuaternionStamped.h"

#include <pcl/common/common.h>

#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>

namespace plt = matplotlibcpp;

#define VIEW_PLOT 0

ros::Publisher pubFullWayPoint;

sensor_msgs::NavSatFix gps_position_;
turbine_segmentation::Waypoints wp;
turbine_segmentation::WaypointsPath wpPath;

float xFlight = 0;
float yFlight = 0;
float zFlight = 0;

std::vector<float> xPath, yPath, zPath;
std::map<std::string, std::string> keywords;

//std::ofstream myfile;
float currentYawGlobal = 0;
float homeAltitude = 0;
int countHits = 0;
int bladesTraversed = 1;

std::vector< std::tuple<float, float, float> > temp;
std::vector< std::tuple<float, float, float> > blade1;
std::vector< std::tuple<float, float, float> > blade2;
std::vector< std::tuple<float, float, float> > blade3;

std::pair<float, float> gpsOffset(float lat, float lon, float delta, float yaw) 
{
	float angle = - (yaw - 90);
	//Use updated radius of earth based on latitude location
	double r1 = 6378.137; //equator r in km
	double r2 = 6356.752; //pole r in km

	double term1 = (r1 * r1 * cos(gps_position_.latitude * M_PI/180));
	double term2 = (r2 * r2 * sin(gps_position_.latitude * M_PI/180));

	double numerator = term1 * term1 + term2 * term2;

	double term3 = r1 * cos(gps_position_.latitude * M_PI/180);
	double term4 = r2 * sin(gps_position_.latitude * M_PI/180);

	double denominator = term3 * term3 + term4 * term4;

	double updatedRadius = sqrt(numerator/denominator) * 1000; //convert to meters
	//std::cout << "RADIUS OF EARTH " << updatedRadius << std::endl; 

    
    //Add the trailing edge gps location. Based on aviation formula
    float offset = delta;  //20m delta from leading edge to trailing edge
    
    //North is 0 degrees for DJI yaw
    float dn = offset * sin(angle * M_PI/180);
    float de = offset * cos(angle * M_PI/180); 
    
	//Coordinate offsets in radians
	float dLat = dn/updatedRadius;
	float dLon = de/(updatedRadius * cos(M_PI * lat/180));
	
	//OffsetPosition, decimal degrees
	float lat0 = lat + dLat * 180/M_PI;
	float lon0 = lon + dLon * 180/M_PI; 
	 
    // Packing two values to return a pair  
    return std::make_pair(lat0, lon0);             
} 

void createWaypointFlight(const std::vector < std::tuple<float, float, float> > & b1, const std::vector < std::tuple<float, float, float> > & b2,
						const std::vector < std::tuple<float, float, float> > & b3, float curYaw)
{
	float origYaw = currentYawGlobal;
	float oppositeYaw = currentYawGlobal >= 0? -180 + currentYawGlobal: currentYawGlobal + 180;
	
	//Each blade path consists of 2 gps coordinates.
	//Begin Waypoint path starting from the end
	//of blade 3

	wp.latitude = std::get<0>(b3[1]);
	wp.longitude = std::get<1>(b3[1]);
	wp.altitude = std::get<2>(b3[1]) + 6;
	wp.yaw = currentYawGlobal;
	wp.cameraPitch = 0;
	wp.cameraYaw = currentYawGlobal;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Second waypoint
	std::pair<float, float> p = gpsOffset(wp.latitude, wp.longitude, 6, currentYawGlobal);
	
	wp.latitude = p.first;
	wp.longitude = p.second;
	wp.altitude = std::get<2>(b3[1]) + 6;
	wp.yaw = oppositeYaw;
	wp.cameraPitch = 0;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Third waypoint
	wp.latitude = p.first;
	wp.longitude = p.second;
	wp.altitude = std::get<2>(b3[1]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = 20;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Fourth waypoint
	std::pair<float, float> p1 = gpsOffset(std::get<0>(b3[0]), std::get<1>(b3[0]), 6, origYaw);
	
	wp.latitude = p1.first;
	wp.longitude = p1.second;
	wp.altitude = std::get<2>(b3[0]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = 20;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Fifth waypoint
	//std::pair<float, float> p2 = gpsOffset(std::get<0>(b3[0]), std::get<1>(b3[0]), 10, origYaw);
	
	//wp.latitude = p2.first;
	//wp.longitude = p2.second;
	//wp.altitude = std::get<2>(b3[0]);
	//wp.yaw = oppositeYaw;
	//wp.cameraPitch = 0;
	
	//wpPath.waypointsArray.push_back(wp);
	
	/////////////////////////////Begin blade 2 below /////////////////////////
	
	//Sixth waypoint
	//std::pair<float, float> p3 = gpsOffset(std::get<0>(b2[0]), std::get<1>(b2[0]), 10, origYaw);
	
	//wp.latitude = p3.first;
	//wp.longitude = p3.second;
	//wp.altitude = std::get<2>(b2[0]);
	//wp.yaw = oppositeYaw;
	//wp.cameraPitch = 0;
	
	//wpPath.waypointsArray.push_back(wp);
	
	//Seventh waypoint
	std::pair<float, float> p4 = gpsOffset(std::get<0>(b2[0]), std::get<1>(b2[0]), 6, origYaw);
	
	wp.latitude = p4.first;
	wp.longitude = p4.second;
	wp.altitude = std::get<2>(b2[0]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = -20;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Eigth waypoint
	std::pair<float, float> p5 = gpsOffset(std::get<0>(b2[1]), std::get<1>(b2[1]), 6, origYaw);
	
	wp.latitude = p5.first;
	wp.longitude = p5.second;
	wp.altitude = std::get<2>(b2[1]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = -20;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Ninth waypoint
	//std::pair<float, float> p6 = gpsOffset(std::get<0>(b2[1]), std::get<1>(b2[1]), 10, origYaw);
	
	//wp.latitude = p6.first;
	//wp.longitude = p6.second;
	//wp.altitude = std::get<2>(b2[1]);
	//wp.yaw = oppositeYaw;
	//wp.cameraPitch = 0;
	
	//wpPath.waypointsArray.push_back(wp);
	
	/////////////////////////Begin last blade
	//Tenth waypoint
	//std::pair<float, float> p7 = gpsOffset(std::get<0>(b1[0]), std::get<1>(b1[0]), 10, origYaw);
	
	//wp.latitude = p7.first;
	//wp.longitude = p7.second;
	//wp.altitude = std::get<2>(b1[0]);
	//wp.yaw = oppositeYaw;
	//wp.cameraPitch = 0;
	
	//wpPath.waypointsArray.push_back(wp);
	
	//Eleventh waypoint
	std::pair<float, float> p8 = gpsOffset(std::get<0>(b1[0]), std::get<1>(b1[0]), 6, origYaw);
	
	wp.latitude = p8.first;
	wp.longitude = p8.second;
	wp.altitude = std::get<2>(b1[0]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = -45;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Twelfth waypoint
	std::pair<float, float> p9 = gpsOffset(std::get<0>(b1[1]), std::get<1>(b1[1]), 6, origYaw);
	
	wp.latitude = p9.first;
	wp.longitude = p9.second;
	wp.altitude = std::get<2>(b1[1]);
	wp.yaw = oppositeYaw;
	wp.cameraPitch = -45;
	wp.cameraYaw = oppositeYaw;
	
	wpPath.waypointsArray.push_back(wp);
	
	/////// Print the computed inspection
	for (int i = 0; i < wpPath.waypointsArray.size(); i++)
	{
		std::cout << wpPath.waypointsArray[i].latitude << " " << wpPath.waypointsArray[i].longitude << " " << wpPath.waypointsArray[i].altitude << std::endl;
	}

	pubFullWayPoint.publish(wpPath);
}

void flight_data(const turbine_segmentation::BladePath::ConstPtr& msg)
{
	if(xPath.size() == 0)
	{
		xPath.push_back(xFlight);
		yPath.push_back(yFlight);
		zPath.push_back(zFlight);
	}
	
	xFlight += msg->bladeX;
	yFlight += msg->bladeY;
	zFlight += msg->bladeZ;
	
	xPath.push_back(xFlight);
	yPath.push_back(yFlight);
	zPath.push_back(zFlight);
	
	//myfile.open ("gpsData.txt", std::ios::out | std::ios::app);
	
	//Add the incoming drone location gps location
    //myfile << std::fixed << std::setprecision(9) << gps_position_.latitude << ", " << gps_position_.longitude << std::endl;
    std::cout << "Adding flight path point" << std::endl;
    temp.push_back(std::make_tuple(gps_position_.latitude, gps_position_.longitude, gps_position_.altitude - homeAltitude));
    
    if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 1)
    {
		std::cout << "First Blade" << std::endl;
		
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		blade1 = temp;
		std::cout << "Lenth of blade 1 " << blade1.size() << std::endl;
		
		blade1.erase(blade1.begin());
		blade1.erase(blade1.begin());
		blade1.erase(blade1.begin());
		
		auto first = blade1.cbegin() + 1;
		auto last = blade1.cbegin() + blade1.size() - 1;
		blade1.erase(first,last);
		
		temp.clear();
		bladesTraversed++;
		
		std::cout << "\n" << std::endl;
		for(int i =0; i < blade1.size(); i++)
		{
			float x = std::get<0>(blade1[i]);
			float y = std::get<1>(blade1[i]);
			float z = std::get<2>(blade1[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		
	}
	else if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 2)
    {
		std::cout << "Second blade" << std::endl;
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		
		blade2 = temp;
		std::cout << "Lenth of blade 2 " << blade2.size() << std::endl;
		
		blade2.erase(blade2.begin());
		auto first = blade2.cbegin() + 1;
		auto last = blade2.cbegin() + blade2.size() - 1;
		blade2.erase(first,last);

		temp.clear();
		bladesTraversed++;
		for(int i =0; i < blade2.size(); i++)
		{
			float x = std::get<0>(blade2[i]);
			float y = std::get<1>(blade2[i]);
			float z = std::get<2>(blade2[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}

	}
	else if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 3)
    {
		std::cout << "Third blade" << std::endl;
		blade3 = temp;
		
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}
		std::cout << "Lenth of blade 3 " << blade2.size() << std::endl;
		
		blade3.erase(blade3.begin());
		auto first = blade3.cbegin() + 1;
		auto last = blade3.cbegin() + blade3.size() - 1;
		blade3.erase(first,last);

		temp.clear();
		bladesTraversed++;

		for(int i =0; i < blade3.size(); i++)
		{
				float x = std::get<0>(blade3[i]);
				float y = std::get<1>(blade3[i]);
				float z = std::get<2>(blade3[i]);
				std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}

		
		createWaypointFlight(blade1, blade2, blade3, currentYawGlobal);
	}
   
		
	//myfile << std::fixed << std::setprecision(9) << lat0 << ", " << lon0 << "\n" << std::endl;
    
    //myfile.close();
	
}

void gpsPositionSubCallback(const sensor_msgs::NavSatFix::ConstPtr& gpsPosition)
{
  countHits++;
  gps_position_ = *gpsPosition;
  if(countHits < 25)
	homeAltitude = gps_position_.altitude;
}

void attitude_data(const geometry_msgs::QuaternionStamped::ConstPtr& attitude_msg)
{
	float x = attitude_msg->quaternion.x;
	float y = attitude_msg->quaternion.y;
	float z = attitude_msg->quaternion.z;
	float w = attitude_msg->quaternion.w;

	float q0 = w;
	float q1 = x;
	float q2 = y;
	float q3 = z;

	float q2sqr = q2 * q2;
	float t0 = -2.0 * (q2sqr + q3 * q3) + 1.0;
	float t1 = 2 * (q1 * q2 + q0 * q3);
	float t2 = -2 * (q1 * q3 - q0 * q2);
	float t3 = 2 * (q2 * q3 + q0 * q1);
	float t4 = -2 * (q1 * q1 + q2sqr) + 1;
	
	t2 = (t2 > 1.0) ? 1.0 : t2;
	t2 = (t2 < -1.0) ? -1.0 : t2;
	
	float pitchQ = asin(t2) * 180/M_PI;
	float rollQ = atan2(t3, t4) * 180/M_PI;
	float yawQ = atan2(t1, t0) * 180/M_PI;
	//std::cout << "yaw " << yawQ << " pitch " << pitchQ << " roll " << rollQ << std::endl;
	currentYawGlobal = yawQ;
}

int main (int argc, char** argv)
{
  	// Initialize ROS
	ros::init (argc, argv, "curvatureOfBlade");
	ros::NodeHandle nh;

	ros::Subscriber bladeCurve = nh.subscribe("/flightPath",1000,flight_data);
	
	ros::Subscriber gpsPositionSub = nh.subscribe("dji_osdk_ros/gps_position", 1000, &gpsPositionSubCallback);
	ros::Subscriber subAttitude = nh.subscribe("/dji_osdk_ros/attitudeRPY",1,attitude_data);

	pubFullWayPoint = nh.advertise<turbine_segmentation::WaypointsPath>("/waypointPath", 1000);

	while(ros::ok())
	{
		if (VIEW_PLOT)
		{
			keywords.insert(std::pair<std::string, std::string>("label", "blade curvature") );
			//plt::clf();	
			plt::plot3(xPath,yPath,zPath,keywords);

			plt::axis("square");
			plt::xlim(-60, 0);
			plt::ylim(0, 30);
			plt::show();
		}
		ros::spinOnce();
	}
	return 0;
}
