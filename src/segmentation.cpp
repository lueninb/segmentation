#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>

#include <math.h>
#include <vector>
#include <numeric>
#include "VectorAverage.h"
#include "matplotlibcpp.h"
#include <turbine_segmentation/BladeAngles.h>
#include <turbine_segmentation/BladeTrackingCommand.h>
#include <string>


/* Livox Lidar Coordinate System
 * +x - forward
 * +y - left
 * +z - up
*/

namespace plt = matplotlibcpp;

#define PI 3.14159265
#define PROXIMITY_ZONE 4
#define VIEW_PLOT 0

ros::Publisher pub;
ros::Publisher pubCentroids;
ros::Publisher pubBladeAngles;

std::vector<std::vector<float> > vec;
std::vector<float> temp;

int validBladeAnglesCount = 0;
double currentState = -1;

bool segmentationService(turbine_segmentation::BladeTrackingCommand::Request &req, turbine_segmentation::BladeTrackingCommand::Response &resp)
{
	currentState = req.state;
	resp.response = true;
	return true;
}

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
	if (currentState == 1)
	{
		// Container for original & filtered data
		pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2; 
		pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
		pcl::PCLPointCloud2 cloud_filtered;

		// Convert to PCL data type
		pcl_conversions::toPCL(*cloud_msg, *cloud);

		// Perform the actual filtering
		pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
		sor.setInputCloud (cloudPtr);
		sor.setLeafSize (0.01, 0.01, 0.01);
		sor.filter (cloud_filtered);

		pcl::PointCloud<pcl::PointXYZ> point_cloud;
		pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloudPtr(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromPCLPointCloud2( cloud_filtered, point_cloud);
		pcl::copyPointCloud(point_cloud, *point_cloudPtr);

		// Creating the KdTree object for the search method of the extraction
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
		tree->setInputCloud(point_cloudPtr);

		std::vector<pcl::PointIndices> cluster_indices;
		pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
		ec.setClusterTolerance(0.045); // 2cm
		ec.setMinClusterSize(250); //100
		ec.setMaxClusterSize(99000000);
		ec.setSearchMethod(tree);
		ec.setInputCloud(point_cloudPtr);
		ec.extract(cluster_indices);

		pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_segmented(new pcl::PointCloud<pcl::PointXYZRGB>);
			

		int j= 0;

		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
		  {
			for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
				{
					pcl::PointXYZRGB point;
					point.x = point_cloudPtr->points[*pit].x;
					point.y = point_cloudPtr->points[*pit].y;
					point.z = point_cloudPtr->points[*pit].z;

					if (j == 0) //Red	#FF0000	(255,0,0)
					 {
						  point.r = 0;
						  point.g = 0;
						  point.b = 255;
					 }
					else if (j == 1) //Lime	#00FF00	(0,255,0)
					 {
						  point.r = 0;
						  point.g = 255;
						  point.b = 0;
					 }
					else if (j == 2) // Blue	#0000FF	(0,0,255)
					 {
						  point.r = 255;
						  point.g = 0;
						  point.b = 0;
					 }
					else if (j == 3) // Yellow	#FFFF00	(255,255,0)
					 {
						  point.r = 255;
						  point.g = 255;
						  point.b = 0;
					 }
					else if (j == 4) //Cyan	#00FFFF	(0,255,255)
					 {
						  point.r = 0;
						  point.g = 255;
						  point.b = 255;
					 }
					else if (j == 5) // Magenta	#FF00FF	(255,0,255)
					 {
						  point.r = 255;
						  point.g = 0;
						  point.b = 255;
					 }
					else if (j == 6) // Olive	#808000	(128,128,0)
					 {
						  point.r = 128;
						  point.g = 128;
						  point.b = 0;
					 }
					else if (j == 7) // Teal	#008080	(0,128,128)
					 {
						  point.r = 0;
						  point.g = 128;
						  point.b = 128;
					 }
					else if (j == 8) // Purple	#800080	(128,0,128)
					 {
						  point.r = 128;
						  point.g = 0;
						  point.b = 128;
					 }
					else
					 {
						  if (j % 2 == 0)
						   {
								point.r = 255 * j / (cluster_indices.size());
								point.g = 128;
								point.b = 50;
						   }
						  else
						   {
								point.r = 0;
								point.g = 255 * j / (cluster_indices.size());
								point.b = 128;
						   }
					 }
					point_cloud_segmented->push_back(point);
				
				}
			j++;
		}

		// Convert to ROS data type
		point_cloud_segmented->header.frame_id = point_cloudPtr->header.frame_id;
		if(point_cloud_segmented->size()) pcl::toPCLPointCloud2(*point_cloud_segmented, cloud_filtered);
		else pcl::toPCLPointCloud2(*point_cloudPtr, cloud_filtered);
		sensor_msgs::PointCloud2 output;
		pcl_conversions::fromPCL(cloud_filtered, output);

		//Calculate the centroid of each cluster
		pcl::PointCloud<pcl::PointXYZ>::Ptr msg (new pcl::PointCloud<pcl::PointXYZ>);

		for (size_t i = 0; i < cluster_indices.size(); i++)
		{
			Eigen::Vector4f centroid;
			pcl::compute3DCentroid(*point_cloudPtr, cluster_indices[i], centroid);
			
			msg->header.frame_id = "livox_frame";
			msg->height = 1;
			msg->width = msg->points.size() + 2;
			msg->points.push_back(pcl::PointXYZ(centroid[0], centroid[1], centroid[2]));
		}
	  
		// Publish the clustered/segmented data
		pub.publish (output);
		
		//Calculate the distance to nearest object
		pcl::PointCloud<pcl::PointXYZ>::Ptr vertices (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromPCLPointCloud2(*cloud, *vertices);
		float minDistanceToTarget = FLT_MAX;
		float currentDist = FLT_MAX;
		float xClosest, yClosest, zClosest = FLT_MAX;
		
		float xTower, yTower, zTower = FLT_MAX;

		for (int idx = 0; idx < vertices->size(); idx++)
		{
				pcl::PointXYZ singlePoint = vertices->points[idx];
				float x = singlePoint._PointXYZ::data[0];
				float y = singlePoint._PointXYZ::data[1];
				float z = singlePoint._PointXYZ::data[2];
				
				//Check +/- vertical zone from drone
				float currentDist = sqrt(x*x + y*y + z*z);
				if (abs(z) < PROXIMITY_ZONE && currentDist != 0.0 && currentDist < minDistanceToTarget)
				{
					minDistanceToTarget = currentDist;
					xClosest = x;
					yClosest = y;
					zClosest = z;				
				}		
		}
		
		//std::cout << "Min Distance " << minDistanceToTarget << std::endl;
		
		//Calculate tower centroid
		for (int i = 0; i < msg->points.size(); i++)
		{
			
			if (msg->points[i]._PointXYZ::data[2] < zTower && msg->points[i]._PointXYZ::data[2] < 0)
			{
				xTower = msg->points[i]._PointXYZ::data[0];
				yTower = msg->points[i]._PointXYZ::data[1];
				zTower = msg->points[i]._PointXYZ::data[2];
			}
		}
		
		msg->points.push_back(pcl::PointXYZ(xClosest, yClosest, zClosest));    
		//Publish the Centroids
		pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
		pubCentroids.publish(msg);
		
		if (VIEW_PLOT)
		{
			//Plot line from Tower to Hub
			std::vector<float> xHubPlot, yHubPlot;
				
			xHubPlot.push_back(0);
			xHubPlot.push_back(0);
			
			yHubPlot.push_back(0);
			yHubPlot.push_back(-10);
			
			
			plt::plot(xHubPlot,yHubPlot, "r-");
			
			plt::axis("square");
			plt::xlim(-10, 10);
			plt::ylim(-10, 10);
		}
		
		//Clear temp angle vector
		temp.clear();
		
		//Calculate angles to each centroid
		for (int i = 0; i < msg->points.size(); i++)
		{
			float xhubVector = 0;
			float yhubVector = yTower - yClosest;
			float zhubVector = zTower - zClosest;
			
			float xBlade = msg->points[i]._PointXYZ::data[0];
			float yBlade = msg->points[i]._PointXYZ::data[1];
			float zBlade = msg->points[i]._PointXYZ::data[2];
			
			//Ignore tower, hub center, and erroneous clusters
			if (xBlade == xClosest && yBlade == yClosest && zBlade == zClosest)
				continue;
				
			if (xBlade == xTower && yBlade == yTower && zBlade == zTower)
				continue;
		
			float distToHub = sqrt((xBlade - xClosest) * (xBlade - xClosest) + (yBlade - yClosest)*(yBlade - yClosest) + (zBlade - zClosest)*(zBlade - 
			zClosest));
			
			if (distToHub < 3)
				continue;
			
			
			float xbladeVector = 0;
			float ybladeVector = yBlade - yClosest;
			float zbladeVector = zBlade - zClosest;
			
			//Calculate dot product of blade with hub vector
			float dotResult = xhubVector * xbladeVector + yhubVector * ybladeVector + zhubVector * zbladeVector;
			
			//Calculate magnitude of vectors
			float hubMag = sqrt(xhubVector * xhubVector + yhubVector * yhubVector + zhubVector * zhubVector);
			float bladeMag = sqrt(xbladeVector * xbladeVector + ybladeVector * ybladeVector	+ zbladeVector * zbladeVector);
			
			float angle = acos(dotResult/ (hubMag * bladeMag)) * 180/PI;
			
			//Conversion to Right hand rule angles
			if (yBlade < yClosest)
				angle = angle + 180;
			else if (yBlade > yClosest)
				angle = 180 - angle;
			temp.push_back(angle);

		}
		
		std::string angleTextbox = "";
		
		//Avoid ambiguity near 12 o'clock, e.g. 4 deg = 364
		if (temp.size() == 3 )
			if (abs(temp[0]) < 7)
				temp[0] = temp[2] + 120;
		
		
		sort(temp.begin(), temp.end());
		//std::cout << "Angles " << temp[0] << " " << temp[1] << " " << temp[2] << std::endl;
		
		if ( temp.size() == 3 && ( abs(temp[2] - temp[1] - 120) <= 10 ) && ( abs(temp[1] - temp[0] - 120) <= 10 ) ) 
		{
			//std::cout << "ENTERED" << std::endl;
			vec.push_back(temp);
			auto averages = GetVectorAverage(vec);
			
			//Find dominant blades and adjust ancillary
			if ( averages[0] < 180)
			{
					if (abs (abs(averages[0] - averages[1]) - 120) <= 4 )
						averages[2] = (averages[2] + 120 + averages[1])/2;
					else if (abs (abs(averages[1] - averages[2]) - 120) <= 4 )
						averages[0] = (averages[0] + averages[1] - 120)/2;
			}
			
			if (VIEW_PLOT)
			{
				for (auto &elem : averages)
				{

					//Add 90 degrees since we start our blade angles at 12 o'clock
					float endy = 5 * sin((90 + elem) * PI/180);
					float endx = 5 * cos((90 + elem) * PI/180);
					
					//Plot line from Tower to Hub
					std::vector<float> xBladePlot, yBladePlot;

					xBladePlot.push_back(0);
					xBladePlot.push_back(endx);

					yBladePlot.push_back(0);
					yBladePlot.push_back(endy);
					
					plt::plot(xBladePlot,yBladePlot, "b-");
				
					angleTextbox = angleTextbox + std::to_string(trunc(round(elem))) + " ";
				
				}
				
				plt::text(-5,7,angleTextbox);
				plt::pause(0.001);
				
				plt::clf();
			}
			
			//Publish blade angles
			turbine_segmentation::BladeAngles bladeMessage;
			bladeMessage.bladeOneAngle = averages[0];
			bladeMessage.bladeTwoAngle = averages[1];
			bladeMessage.bladeThreeAngle = averages[2];
			bladeMessage.minDistanceToTarget = minDistanceToTarget;
			bladeMessage.xHub = xClosest;
			bladeMessage.yHub = yClosest;
			bladeMessage.zHub = zClosest;
			bladeMessage.xTower = xTower;
			bladeMessage.yTower = yTower;
			bladeMessage.zTower = zTower;		
			
			pubBladeAngles.publish(bladeMessage);
			validBladeAnglesCount++;
		}
	}
}

int main (int argc, char** argv)
{
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
  	
  // Initialize ROS
  ros::init (argc, argv, "turbine_segmentation");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/livox/lidar", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);
  
  // Create a ROS publisher for the centroid point clouds
  pubCentroids = nh.advertise<sensor_msgs::PointCloud2> ("centroids", 1);
  
  // Create a ROS publisher for the 3 blade angles
  pubBladeAngles = nh.advertise<turbine_segmentation::BladeAngles>("turbineData", 1000);
  
  ros::ServiceServer server = nh.advertiseService("segmentationService",segmentationService);
  
  ros::ServiceClient client = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("currentState");

  while(ros::ok())
  {
		//ros::Duration(1.0).sleep();
		std::cout << "Seg Current State " << currentState << std::endl;
		if (currentState == 1)
		{	
			std::cout << validBladeAnglesCount << std::endl;
			if (validBladeAnglesCount == 5)
			{
				turbine_segmentation::BladeTrackingCommand srv;
				srv.request.initBladeAngle = 0;
				srv.request.distance = 0;
				srv.request.state = 2;
				if (client.call(srv))
				{
					std::cout << "Message sent " << std::endl;
					sub = nh.subscribe ("/nothing", 1, cloud_cb);
					currentState = 2;
				}
			}
		}
		else
			std::cout << "OFF STATE" << std::endl;
		 // Spin
		ros::spinOnce();
	  
  }
}
