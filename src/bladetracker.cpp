#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <math.h>
#include <vector>
#include <numeric>
#include "VectorAverage.h"
#include "matplotlibcpp.h"
#include <turbine_segmentation/BladeAngles.h>
#include <turbine_segmentation/BladePath.h>
#include <turbine_segmentation/BladeTrackingCommand.h>
#include <turbine_segmentation/BladeTrackingAngle.h>
#include "geometry_msgs/QuaternionStamped.h"

#include <cmath>
#include <algorithm>
#include <iostream>

/* Livox Lidar Coordinate System
 * +x - forward
 * +y - left
 * +z - up
*/

namespace plt = matplotlibcpp;

#define PI 3.14159265
#define VIEW_PLOT 0

ros::Publisher pubCentroids;
ros::Publisher pubBladeTrackingPath;

std::vector<std::vector<float> > vec;
std::vector<float> temp;

Eigen::Matrix3f m = Eigen::Matrix3f::Identity();

float angleDiff(float a, float b)
{
	float c = ((int)b - (int)a) % 360;

	if (c > 180)
		c -= 360;
	return c;
}

float convertAngle(float y, float yCenter, float angle )
{
	//Conversion to Right hand rule angles
	if (y <= yCenter)
		angle = 360 - angle;

	return angle;
}

double initialBladeAngle = 0;
double xCommand = 0;
double yCommand = 0;
double zCommand = 0;
bool publishTracking = true;
int commandIndex = 0;

double currentState = -1;
bool endOfTracking = false;
bool CONVERGE = true;

bool bladeCommands(turbine_segmentation::BladeTrackingCommand::Request &req, turbine_segmentation::BladeTrackingCommand::Response &resp)
{
	initialBladeAngle = req.initBladeAngle;
	currentState = req.state;
	commandIndex = 0;
	if (commandIndex == 0)
	{
		xCommand = -15 * sin(initialBladeAngle * PI/180);
		yCommand = 0.0;
		zCommand = 15 * cos(initialBladeAngle * PI/180);	
		
		//Round commands to nearest tenth of a meter
		xCommand = round(10 * xCommand) / 10;
		zCommand = round(10 * zCommand) / 10;
	}
	//Publish initial command
	turbine_segmentation::BladePath initBladePathMessage;
	initBladePathMessage.bladeX = xCommand;
	initBladePathMessage.bladeY = yCommand;
	initBladePathMessage.bladeZ = zCommand;
	
	if (publishTracking)
		pubBladeTrackingPath.publish(initBladePathMessage);
	resp.xCmd = xCommand;
	resp.yCmd = yCommand;
	resp.zCmd = zCommand;
	resp.response = true;
	
	std::cout << " Commands " << xCommand << " , " << yCommand << ", " << zCommand << std::endl;
	commandIndex = 1;	
	return true;
}

void attitude_data(const geometry_msgs::QuaternionStamped::ConstPtr& attitude_msg)
{
	float x = attitude_msg->quaternion.x;
	float y = attitude_msg->quaternion.y;
	float z = attitude_msg->quaternion.z;
	float w = attitude_msg->quaternion.w;

	Eigen::Quaternionf q;
	q.x() = x;
	q.y() = y;
	q.z() = z;
	q.w() = w; 

	float q0 = w;
	float q1 = x;
	float q2 = y;
	float q3 = z;

	float q2sqr = q2 * q2;
	float t0 = -2.0 * (q2sqr + q3 * q3) + 1.0;
	float t1 = 2 * (q1 * q2 + q0 * q3);
	float t2 = -2 * (q1 * q3 - q0 * q2);
	float t3 = 2 * (q2 * q3 + q0 * q1);
	float t4 = -2 * (q1 * q1 + q2sqr) + 1;
	
	t2 = (t2 > 1.0) ? 1.0 : t2;
	t2 = (t2 < -1.0) ? -1.0 : t2;
	
	
	float pitchQ = asin(t2) * 180/M_PI;
	float rollQ = atan2(t3, t4) * 180/M_PI;
	float yawQ = atan2(t1, t0) * 180/M_PI;
	//std::cout << "yaw " << yawQ << " pitch " << pitchQ << " roll " << rollQ << std::endl;
	
	m << cos(rollQ * M_PI/180), -sin(rollQ * M_PI/180), 0,
			 sin(rollQ * M_PI/180), cos(rollQ * M_PI/180), 0,
			 0,0,1;

}

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
	if(currentState == 3 || currentState == 5 || currentState == 6)
	{
		usleep(1);
		// Container for original & filtered data
		pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2; 
		pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
		pcl::PCLPointCloud2 cloud_filtered;

		// Convert to PCL data type
		pcl_conversions::toPCL(*cloud_msg, *cloud);
		
		// Perform the actual filtering
		pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
		sor.setInputCloud (cloudPtr);
		sor.setLeafSize (0.1, 0.1, 0.1);
		sor.filter (cloud_filtered);

		pcl::PointCloud<pcl::PointXYZ> point_cloud;
		pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloudPtr(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromPCLPointCloud2( cloud_filtered, point_cloud);
		pcl::copyPointCloud(point_cloud, *point_cloudPtr);

		vec.clear();
		
		for (int idx = 0; idx < point_cloud.size(); idx++)
		{
				temp.clear();
				if (point_cloud.size() > 2	)
				{
					pcl::PointXYZ singlePoint = point_cloud.points[idx];
					float x = singlePoint._PointXYZ::data[0];
					float y = singlePoint._PointXYZ::data[1];
					float z = singlePoint._PointXYZ::data[2];

					if (y == 0 && z == 0)
						continue;

					float distLidar = sqrt ( x*x + y*y + z*z);

					if ( distLidar > 20)
						continue;
					// Change due to lidar coordinate system
					temp.push_back(x);
					temp.push_back(y);
					temp.push_back(z);	
				}
				vec.push_back(temp);
		}
		if (vec.size() > 100)
		{
			auto averages = GetVectorAverage(vec);
			//std::cout << "Average " << averages[1] << " " << averages[2] << std::endl;
			
			std::vector<float> xAveg, yAveg;
			xAveg.push_back(averages[1]);
			yAveg.push_back(averages[2]);

			//Store centroid of blade cloud
			pcl::PointCloud<pcl::PointXYZ>::Ptr msg (new pcl::PointCloud<pcl::PointXYZ>);
			msg->header.frame_id = "livox_frame";
			msg->height = 1;
			msg->width = msg->points.size() + 1;
			msg->points.push_back(pcl::PointXYZ(0, averages[1], averages[2]));
			pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
			pubCentroids.publish(msg);
			
			
			//Plot points
			std::vector<float> xPts, yPts;
			pcl::PointCloud<pcl::PointXYZ>::Ptr filteredPoints (new pcl::PointCloud<pcl::PointXYZ>);
			pcl::PointCloud<pcl::PointXYZ>::Ptr filteredPointsFlat (new pcl::PointCloud<pcl::PointXYZ>);
			pcl::PointCloud<pcl::PointXYZ>::Ptr filteredPointsFlatOutliers (new pcl::PointCloud<pcl::PointXYZ>);

			for (int idx = 0; idx < vec.size(); idx++)
			{	
				Eigen::Vector3f v(vec[idx][1], vec[idx][2], 0);

				Eigen::Vector3f rollCorrected = m * v;
			
				xPts.push_back(rollCorrected(0));
				yPts.push_back(rollCorrected(1));	
				
				filteredPoints->points.push_back(pcl::PointXYZ(vec[idx][0], vec[idx][1], vec[idx][2]));
				filteredPointsFlatOutliers->points.push_back(pcl::PointXYZ(0, rollCorrected(0), rollCorrected(1)));
			}
			
			//Apply Outlier Removal to reduce noise
			pcl::RadiusOutlierRemoval<pcl::PointXYZ> outrem;
			outrem.setInputCloud(filteredPointsFlatOutliers);
			outrem.setRadiusSearch(0.8);
			outrem.setMinNeighborsInRadius (2);
			outrem.setKeepOrganized(true);
			outrem.filter (*filteredPointsFlat);
			
			//Find centroid of cloud and use that for (x, y) of line
			Eigen::Vector4f centroid;
			pcl::compute3DCentroid(*filteredPointsFlat, centroid);

			Eigen::Vector4f centroidDepth;
			pcl::compute3DCentroid(*filteredPoints, centroidDepth);

			//std::cout << "Size " << filteredPointsFlat->size() << std::endl;
			//std::cout << "Centroid For Depth " << centroidDepth[0] << " " << centroidDepth[1] << " " << centroidDepth[2] << std::endl;
			//std::cout << "Centroid " << centroid[0] << " " << centroid[1] << " " << centroid[2] << std::endl;

			Eigen::Matrix3f covariance;
			//computeCovarianceMatrixNormalized(*cloudSegmented, pcaCentroid, covariance);
			computeCovarianceMatrixNormalized(*filteredPointsFlat, centroid, covariance);
			Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
			Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
			eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));  

			// Transform the original cloud to the origin where the principal components correspond to the axes.
			Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
			projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
			projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * centroid.head<3>());
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
			pcl::transformPointCloud(*filteredPointsFlat, *cloudPointsProjected, projectionTransform);
			

			// Get the minimum and maximum points of the transformed cloud.
			pcl::PointXYZ minPoint, maxPoint;
			pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
			const Eigen::Vector3f meanDiagonal = 0.5f*(maxPoint.getVector3fMap() + minPoint.getVector3fMap());
			

			const Eigen::Quaternionf bboxQuaternion(eigenVectorsPCA); //Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
			const Eigen::Vector3f bboxTransform = eigenVectorsPCA * meanDiagonal + centroid.head<3>();


			Eigen::Matrix4f Trans; // The Transformation Matrix
			Trans.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
			Trans.block<3,3>(0,0) = bboxQuaternion.normalized().toRotationMatrix();
			Trans.block<3,1>(0,3) = bboxTransform;
			

			Eigen::Vector4f upper(maxPoint.x, maxPoint.y, maxPoint.z, 1);
			Eigen::Vector4f newUpper = Trans * upper;
			
			Eigen::Vector4f btmRight(maxPoint.x, maxPoint.y, minPoint.z, 1);
			Eigen::Vector4f newbtmRight = Trans * btmRight;
			
			Eigen::Vector4f lower(minPoint.x, minPoint.y, minPoint.z, 1);
			Eigen::Vector4f newLower = Trans * lower;
			
			Eigen::Vector4f topLeft(minPoint.x, minPoint.y, maxPoint.z, 1);
			Eigen::Vector4f newtopLeft = Trans * topLeft;
			
			//Calculate the two candidate new command locations
			float yCandidateOne = (newLower[1] + newbtmRight[1])/2;
			float zCandidateOne = (newLower[2] + newbtmRight[2])/2;
			
			float yCandidateTwo = (newUpper[1] + newtopLeft[1])/2;
			float zCandidateTwo = (newUpper[2] + newtopLeft[2])/2;
			
			//Caculate angles to the two candidates
			float xCentral = 0;
			float yCentral = 0;
			float zCentral = 1;  //Unit vector along y-axis
			
			float xCanOne = 0;
			float yCanOne = yCandidateOne - averages[1];
			float zCanOne = zCandidateOne - averages[2];	
			
			//Calculate dot product of candidate with central point vector
			float dotResult = xCentral * xCanOne + yCentral * yCanOne + zCentral * zCanOne;

			//Calculate magnitude of vectors
			float centralMag = sqrt(xCentral * xCentral + yCentral * yCentral + zCentral * zCentral);
			float candidateOneMag = sqrt(xCanOne * xCanOne + yCanOne * yCanOne	+ zCanOne * zCanOne);
			float angleOne = acos(dotResult/ (centralMag * candidateOneMag)) * 180/PI;
			
			float xCanTwo = 0;
			float yCanTwo = yCandidateTwo - averages[1];
			float zCanTwo = zCandidateTwo - averages[2];	
			
			//Calculate dot product of candidate with central point vector
			float dotResultTwo = xCentral * xCanTwo + yCentral * yCanTwo + zCentral * zCanTwo;

			//Calculate magnitude of vectors
			float candidateTwoMag = sqrt(xCanTwo * xCanTwo + yCanTwo * yCanTwo	+ zCanTwo * zCanTwo);
			float angleTwo = acos(dotResultTwo/ (centralMag * candidateTwoMag)) * 180/PI;
			
			//std::cout << "AngleOne " << angleOne << " AngleTwo " << angleTwo << std::endl;
			angleOne = convertAngle(yCandidateOne, averages[1], angleOne);
			angleTwo = convertAngle(yCandidateTwo, averages[1], angleTwo);
			
			//std::cout << "AngleOneNew " << angleOne << " AngleTwonNew " << angleTwo << std::endl;
				
			std::vector<float> xCmd, yCmd;
			
			if (abs(angleDiff(initialBladeAngle, angleTwo)) < 20 )
			{
				xCmd.push_back(yCandidateTwo);
				yCmd.push_back(zCandidateTwo);
				
				xCommand = - ( yCandidateTwo - averages[1] );
				yCommand = trunc(round(10 * centroidDepth[0])/10 - 15);         
				zCommand = zCandidateTwo - averages[2];
			}
			else if (abs(angleDiff(initialBladeAngle, angleOne)) < 20 )
			{
				xCmd.push_back(yCandidateOne);
				yCmd.push_back(zCandidateOne);
				
				xCommand = - ( yCandidateOne - averages[1] );
				yCommand = trunc(round(10 * centroidDepth[0])/10 - 15);
				zCommand = zCandidateOne - averages[2];
			}
			
			//Update: Converge to initial center point of blade
			if (CONVERGE)
			{
				xCommand = round(10 * averages[1]) / 10;
				zCommand = round(10 * averages[2]) / 10;
				//CONVERGE = false;
			}
			else
			{			
				//Round commands to nearest tenth of a meter
				xCommand = round(10 * xCommand) / 10;
				zCommand = round(10 * zCommand) / 10;
			}
			
			float travelDistance = sqrt(xCommand * xCommand + yCommand*yCommand + zCommand*zCommand);
			//std::cout << "Travel " << travelDistance << std::endl;

			//Publish blade path commands
			turbine_segmentation::BladePath bladePathMessage;
			bladePathMessage.bladeX = xCommand;
			bladePathMessage.bladeY = yCommand;
			bladePathMessage.bladeZ = zCommand;

			//std::cout << " SIZE " << filteredPointsFlat->size();
			if (commandIndex > 0 && (travelDistance > 2 || CONVERGE) && filteredPointsFlat->size() > 150)
			{
				CONVERGE = false;
				pubBladeTrackingPath.publish(bladePathMessage);
				publishTracking = false; //so not to publish initial cmd again
			}
			else if (commandIndex != 0 && filteredPointsFlat->size() <= 100)
			{		
				ros::NodeHandle nhquit;
				ros::ServiceClient client = nhquit.serviceClient<turbine_segmentation::BladeTrackingAngle>("toggleBladeTracking");
				turbine_segmentation::BladeTrackingAngle srv;
				srv.request.bladeAngle = 0;
				//client.call(srv);
			}
			
			if (VIEW_PLOT)
			{
				//Plot bounding box sides
				std::vector<float> xUpper, yUpper;
				xUpper.push_back(newLower[1]);
				xUpper.push_back(newbtmRight[1]);

				yUpper.push_back(newLower[2]);
				yUpper.push_back(newbtmRight[2]);
				
				std::vector<float> xLower, yLower;
				xLower.push_back(newUpper[1]);
				xLower.push_back(newtopLeft[1]);
					
				yLower.push_back(newUpper[2]);
				yLower.push_back(newtopLeft[2]);
				
				plt::plot(xCmd,yCmd,"co");
				plt::plot(xPts,yPts,"r.");
				plt::plot(xAveg,yAveg,"b.");
				plt::plot(xUpper,yUpper,"k-");
				plt::plot(xLower,yLower,"g-");
				
				plt::axis("square");
				plt::xlim(-10, 10);
				plt::ylim(-10, 10);
				plt::pause(0.001);
					
				plt::clf();	
			}
		}
		else //End of tracking so move to next state
		{
			//if (commandIndex > 0 && !endOfTracking)
			//{
			//	turbine_segmentation::BladePath bladePathMessageNull;
			//	bladePathMessageNull.bladeX = 0;
			//	bladePathMessageNull.bladeY = 0;
			//	bladePathMessageNull.bladeZ = 0;
			//	pubBladeTrackingPath.publish(bladePathMessageNull);	
				endOfTracking = true;
				//commandIndex = 0;
			//}
		}
	}
}

int main (int argc, char** argv)
{
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
  	
  // Initialize ROS
  ros::init (argc, argv, "bladetracker");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/livox/lidar", 10, cloud_cb);
  
   // Create a ROS publisher for the centroid point clouds
  pubCentroids = nh.advertise<sensor_msgs::PointCloud2> ("centroids", 1);
  
  // Create ROS publisher for blade path tracking the blade
  pubBladeTrackingPath = nh.advertise<turbine_segmentation::BladePath>("bladePath", 1000);
  
  ros::ServiceServer server = nh.advertiseService("bladeTrackerService", bladeCommands);
  
  ros::Subscriber subAttitude = nh.subscribe("/dji_osdk_ros/attitudeRPY",1000,attitude_data);
  
  ros::ServiceClient client = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("currentState");

  while(ros::ok())
  {
		//ros::Duration(1.0).sleep();
		std::cout << "BT Current State " << currentState << " Current Angle " << initialBladeAngle << std::endl;
		if (currentState == 3 || currentState == 5 || currentState == 6)
		{	
			if (endOfTracking)
			{
				publishTracking = true;
				CONVERGE = true;
				//commandIndex = 0;
				vec.clear();
				temp.clear();
				m = Eigen::Matrix3f::Identity();
				
				turbine_segmentation::BladeTrackingCommand srv;
				srv.request.initBladeAngle = initialBladeAngle;
				srv.request.distance = 0;
				srv.request.state = 4;
				if (currentState == 6)
					srv.request.state = 7;
				if (client.call(srv))
				{
					std::cout << "Message sent " << std::endl;
					currentState = 4;
				}
				endOfTracking = false;
			}
		}
		else
			std::cout << "OFF STATE" << std::endl;
		 // Spin
		ros::spinOnce();
	  
  }
}
