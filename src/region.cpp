#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/segmentation/region_growing.h>




/* Livox Lidar Coordinate System
 * +x - forward
 * +y - left
 * +z - up
*/

#define PROXIMITY_ZONE 2

ros::Publisher pub;
ros::Publisher pubCentroids;


void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
  // Container for original & filtered data
  pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2; 
  pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
  pcl::PCLPointCloud2 cloud_filtered;

  // Convert to PCL data type
  pcl_conversions::toPCL(*cloud_msg, *cloud);
  
  pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
  sor.setInputCloud (cloudPtr);
  sor.setLeafSize (0.012, 0.012, 0.012);
  sor.filter (cloud_filtered);
  
  pcl::PointCloud<pcl::PointXYZ> point_cloud;
  pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloudPtr(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2( cloud_filtered, point_cloud);
  pcl::copyPointCloud(point_cloud, *point_cloudPtr);
  
  
  pcl::search::Search<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
  pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> normal_estimator;
  normal_estimator.setSearchMethod (tree);
  normal_estimator.setInputCloud (point_cloudPtr);
  normal_estimator.setKSearch (50);
  
  int nr_cores = boost::thread::hardware_concurrency();
  normal_estimator.setNumberOfThreads(4);

  
  normal_estimator.compute (*normals);
  
  std::cout << "here" << std::endl;
  


  pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
  reg.setMinClusterSize (50);
  reg.setMaxClusterSize (1000000);
  reg.setSearchMethod (tree);
  reg.setNumberOfNeighbours (30);
  reg.setInputCloud (point_cloudPtr);
  //reg.setIndices (indices);
  reg.setInputNormals (normals);
  reg.setSmoothnessThreshold (10.0 / 180.0 * M_PI);
  reg.setCurvatureThreshold (20.0);
  
  std::vector <pcl::PointIndices> clusters;
  reg.extract (clusters);

  std::cout << "Number of clusters is equal to " << clusters.size () << std::endl;


  pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_segmented(new pcl::PointCloud<pcl::PointXYZRGB>);
        

  int j= 0;
 
  for (std::vector<pcl::PointIndices>::const_iterator it = clusters.begin(); it != clusters.end(); ++it)
	  {
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
		    {
                pcl::PointXYZRGB point;
                point.x = point_cloudPtr->points[*pit].x;
                point.y = point_cloudPtr->points[*pit].y;
                point.z = point_cloudPtr->points[*pit].z;

                if (j == 0) //Red	#FF0000	(255,0,0)
			     {
				      point.r = 0;
				      point.g = 0;
				      point.b = 255;
			     }
			    else if (j == 1) //Lime	#00FF00	(0,255,0)
			     {
				      point.r = 0;
				      point.g = 255;
				      point.b = 0;
			     }
			    else if (j == 2) // Blue	#0000FF	(0,0,255)
			     {
				      point.r = 255;
				      point.g = 0;
				      point.b = 0;
			     }
			    else if (j == 3) // Yellow	#FFFF00	(255,255,0)
			     {
				      point.r = 255;
				      point.g = 255;
				      point.b = 0;
			     }
			    else if (j == 4) //Cyan	#00FFFF	(0,255,255)
			     {
				      point.r = 0;
				      point.g = 255;
				      point.b = 255;
			     }
			    else if (j == 5) // Magenta	#FF00FF	(255,0,255)
			     {
				      point.r = 255;
				      point.g = 0;
				      point.b = 255;
			     }
			    else if (j == 6) // Olive	#808000	(128,128,0)
		     	 {
				      point.r = 128;
				      point.g = 128;
				      point.b = 0;
			     }
			    else if (j == 7) // Teal	#008080	(0,128,128)
			     {
				      point.r = 0;
				      point.g = 128;
				      point.b = 128;
			     }
			    else if (j == 8) // Purple	#800080	(128,0,128)
		     	 {
				      point.r = 128;
				      point.g = 0;
				      point.b = 128;
			     }
			    else
		   	     {
				      if (j % 2 == 0)
				       {
					        point.r = 255 * j / (clusters.size());
					        point.g = 128;
					        point.b = 50;
				       }
				      else
				       {
					        point.r = 0;
					        point.g = 255 * j / (clusters.size());
					        point.b = 128;
				       }
                 }
                point_cloud_segmented->push_back(point);
			
            }
        j++;
    }
  std::cerr<< "segemnted:  " << (int)point_cloud_segmented->size() << "\n";
  std::cerr<< "origin:     " << (int)point_cloudPtr->size() << "\n";
  // Convert to ROS data type
  point_cloud_segmented->header.frame_id = point_cloudPtr->header.frame_id;
  if(point_cloud_segmented->size()) pcl::toPCLPointCloud2(*point_cloud_segmented, cloud_filtered);
  else pcl::toPCLPointCloud2(*point_cloudPtr, cloud_filtered);
  sensor_msgs::PointCloud2 output;
  pcl_conversions::fromPCL(cloud_filtered, output);
  
  //Calculate the centroid of each cluster
  pcl::PointCloud<pcl::PointXYZ>::Ptr msg (new pcl::PointCloud<pcl::PointXYZ>);
  
  for (size_t i = 0; i < clusters.size(); i++)
  {
		Eigen::Vector4f centroid;
		pcl::compute3DCentroid(*point_cloudPtr, clusters[i], centroid);
		std::cout << centroid[0] << " " << centroid[1] << " " << centroid[2] << " " << centroid[3] << std::endl;

        
        msg->header.frame_id = "livox_frame";
        msg->height = 1;
        msg->width = msg->points.size() + 1;
        msg->points.push_back(pcl::PointXYZ(centroid[0], centroid[1], centroid[2]));
  }
  
    //Publish the Centroids
    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
	pubCentroids.publish(msg);
  
	// Publish the clustered/segmented data
	pub.publish (output);
	
	//Calculate the distance to nearest object
	pcl::PointCloud<pcl::PointXYZ>::Ptr vertices (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromPCLPointCloud2(*cloud, *vertices);
	float minDistanceToTarget = FLT_MAX;
	float currentDist = FLT_MAX;

	for (int idx = 0; idx < vertices->size(); idx++)
	{
			pcl::PointXYZ singlePoint = vertices->points[idx];
			float x = singlePoint._PointXYZ::data[0];
			float y = singlePoint._PointXYZ::data[1];
			float z = singlePoint._PointXYZ::data[2];
			
			//Check +/- 2m vertical zone from drone
			float currentDist = sqrt(x*x + y*y + z*z);
			if (abs(z) < PROXIMITY_ZONE && currentDist != 0.0 && currentDist < minDistanceToTarget)
				minDistanceToTarget = currentDist;
			
	}
	std::cout << "Min Dist: " << minDistanceToTarget << std::endl;


}

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "turbine_segmentation");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/livox/lidar", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);
  
  // Create a ROS publisher for the centroid point clouds
  pubCentroids = nh.advertise<sensor_msgs::PointCloud2> ("centroids", 1);

  // Spin
  ros::spin ();
}
