
/** Testing basic takeoff, flight path and landing
 * May 30, 2020 
 * 
 */

//INCLUDE
#include <ros/ros.h>

#include <turbine_segmentation/BladeAngles.h>
#include <turbine_segmentation/BladePath.h>

#include <turbine_segmentation/BladeTrackingAngle.h>
#include <turbine_segmentation/BladeTrackingCommand.h>

#include <iostream>
#include <stdlib.h>

#include <pcl/common/common.h>
#include "geometry_msgs/QuaternionStamped.h"

int x = 2;
int y = 2;
int z = 2;

int main(int argc, char** argv)
{

	ros::init(argc, argv, "triangle_node");
	ros::NodeHandle nh;
	
	ros::Publisher pubBladeTrackingPath = nh.advertise<turbine_segmentation::BladePath>("bladePath", 1);
	

	ros::Duration(1.0).sleep();
	turbine_segmentation::BladePath bladePathMessage;
	bladePathMessage.bladeX = 0;
	bladePathMessage.bladeY = 0;
	bladePathMessage.bladeZ = 40;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(22.0).sleep();
	
	bladePathMessage.bladeX = 0;
	bladePathMessage.bladeY = 0;
	bladePathMessage.bladeZ = 0;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(3.0).sleep();	

	//turbine_segmentation::BladePath Message;
	//Message.bladeX = 0;
	//Message.bladeY = 0;
	//Message.bladeZ = 0;
	//pubBladeTrackingPath.publish(Message);
	//ros::Duration(2.0).sleep();

	bladePathMessage.bladeX = 0;
	bladePathMessage.bladeY = 10;
	bladePathMessage.bladeZ = 0;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(8).sleep();

	for (int i = 0; i < 20; i++)
	{
		ros::Duration(4.0).sleep();
		std::cout << "Up" << std::endl;
		bladePathMessage.bladeX = 0;
		bladePathMessage.bladeY = 0;
		bladePathMessage.bladeZ = 3;
		pubBladeTrackingPath.publish(bladePathMessage);
	}

	std::cout << "Going home" << std::endl;
	ros::Duration(10).sleep();	
	bladePathMessage.bladeX = 100;
	bladePathMessage.bladeY = 100;
	bladePathMessage.bladeZ = 100;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(28.0).sleep();


	for (int i = 0; i < 20; i++)
	{
		ros::Duration(4.0).sleep();
		std::cout << "Left" << std::endl;
		bladePathMessage.bladeX = -3;
		bladePathMessage.bladeY = 0;
		bladePathMessage.bladeZ = -3;
		pubBladeTrackingPath.publish(bladePathMessage);
	}
	std::cout << "Going home" << std::endl;
	ros::Duration(12.0).sleep();
	bladePathMessage.bladeX = 100;
	bladePathMessage.bladeY = 100;
	bladePathMessage.bladeZ = 100;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(34.0).sleep();
	
	
	for (int i = 0; i < 20; i++)
	{
		ros::Duration(4.0).sleep();
		std::cout << "Right" << std::endl;
		bladePathMessage.bladeX = 3;
		bladePathMessage.bladeY = 0;
		bladePathMessage.bladeZ = -3;
		pubBladeTrackingPath.publish(bladePathMessage);
	}
	std::cout << "Starting inspection" << std::endl;
	ros::Duration(12).sleep();
	bladePathMessage.bladeX = 500;
	bladePathMessage.bladeY = 500;
	bladePathMessage.bladeZ = 500;
	pubBladeTrackingPath.publish(bladePathMessage);
	ros::Duration(40.0).sleep();
	

	ros::spinOnce();
	
	return 0;
}



