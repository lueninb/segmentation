# Turbine segmentation

This contains a brief guide how to install / run the code.

## Installation instructions
The tools require full ROS installation. The installation assumes you have Ubuntu 16.04 LTS [ROS Kinetic] or Ubntu 14.04 LTS [ROS Indigo].

## Install ROS:
Please refer to http://wiki.ros.org/kinetic/Installation/Ubuntu

Download the source tree into your catkin workspace (here we assume ~/catkin_ws):


## Compile the source

`$ cd ~/catkin_ws`

`$ catkin_make --pkg turbine_segmentation

## Then run:

$ rosrun turbine_segmentation segmentation

## Subscribe to topic /turbineData to retrieve blade angles and additional data
