#include <ros/ros.h>

#include <math.h>
#include <vector>
#include <numeric>
#include <tuple>

#include "matplotlibcpp.h"
#include <turbine_segmentation/BladePath.h>
#include <turbine_segmentation/BladeAngles.h>
#include <turbine_segmentation/Waypoints.h>
#include <turbine_segmentation/WaypointsPath.h>
#include <sensor_msgs/NavSatFix.h>
#include "geometry_msgs/QuaternionStamped.h"

#include <pcl/common/common.h>

#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>

namespace plt = matplotlibcpp;

#define VIEW_PLOT 0

ros::Publisher pubFullWayPoint;

sensor_msgs::NavSatFix gps_position_;
turbine_segmentation::Waypoints wp;
turbine_segmentation::WaypointsPath wpPath;

float xFlight = 0;
float yFlight = 0;
float zFlight = 0;

std::vector<float> xPath, yPath, zPath;
std::map<std::string, std::string> keywords;

//std::ofstream myfile;
float currentYawGlobal = 0;
float homeAltitude = 0;
int countHits = 0;
int bladesTraversed = 1;

std::vector< std::tuple<float, float, float> > temp;
std::vector< std::tuple<float, float, float> > blade1;
std::vector< std::tuple<float, float, float> > blade2;
std::vector< std::tuple<float, float, float> > blade3;

double bladeOneAngle = 0;
double bladeTwoAngle = 0;
double bladeThreeAngle = 0;

std::pair<float, float> gpsOffset(float lat, float lon, float delta, float yaw) 
{
	float angle = - (yaw - 90);
	//Use updated radius of earth based on latitude location
	double r1 = 6378.137; //equator r in km
	double r2 = 6356.752; //pole r in km

	double term1 = (r1 * r1 * cos(gps_position_.latitude * M_PI/180));
	double term2 = (r2 * r2 * sin(gps_position_.latitude * M_PI/180));

	double numerator = term1 * term1 + term2 * term2;

	double term3 = r1 * cos(gps_position_.latitude * M_PI/180);
	double term4 = r2 * sin(gps_position_.latitude * M_PI/180);

	double denominator = term3 * term3 + term4 * term4;

	double updatedRadius = sqrt(numerator/denominator) * 1000; //convert to meters
	//std::cout << "RADIUS OF EARTH " << updatedRadius << std::endl; 

    
    //Add the trailing edge gps location. Based on aviation formula
    float offset = delta;  //20m delta from leading edge to trailing edge
    
    //North is 0 degrees for DJI yaw
    float dn = offset * sin(angle * M_PI/180);
    float de = offset * cos(angle * M_PI/180); 
    
	//Coordinate offsets in radians
	float dLat = dn/updatedRadius;
	float dLon = de/(updatedRadius * cos(M_PI * lat/180));
	
	//OffsetPosition, decimal degrees
	float lat0 = lat + dLat * 180/M_PI;
	float lon0 = lon + dLon * 180/M_PI; 
	 
    // Packing two values to return a pair  
    return std::make_pair(lat0, lon0);             
} 

turbine_segmentation::WaypointsPath trailingEdge(const std::vector < std::tuple<float, float, float> > & blade)
{
	float oppositeYaw = currentYawGlobal >= 0? -180 + currentYawGlobal: currentYawGlobal + 180;
	float size = blade.size();
	float max_chord = 3.9;
	float increment = max_chord/size;
	float adjustment = 0;
	
	turbine_segmentation::WaypointsPath tempPath;
	for(int i = size-1; i>= 0; i--)
	{
		std::pair<float, float> p = gpsOffset(std::get<0>(blade[i]), std::get<1>(blade[i]), 20 + adjustment, currentYawGlobal);
		wp.latitude = p.first;
		wp.longitude = p.second;
		wp.altitude = std::get<2>(blade[i]);
		wp.yaw = oppositeYaw;
		wp.cameraPitch = -40; //extended gimbal is level at pitch of -40
		
		float newCameraYaw = oppositeYaw - 30; //Gimbal yaw is offset 30 degrees
		if (newCameraYaw < -180)
			newCameraYaw = 180 - abs(180 + newCameraYaw);
			
		wp.cameraYaw = newCameraYaw;

		tempPath.waypointsArray.push_back(wp);
		
		if(i <= floor(0.1 * (size - 1)))  //Max chord at roughly 90% from tip
			adjustment -= increment;
		else
			adjustment += increment;
	}
	std::cout << "Printing the TRAILING path" << std::endl;
	for (int i = 0; i < tempPath.waypointsArray.size(); i++)
	{
			std::cout << tempPath.waypointsArray[i].latitude << " " << tempPath.waypointsArray[i].longitude << " " << \
			tempPath.waypointsArray[i].altitude << " " << tempPath.waypointsArray[i].yaw << \
			 " " << tempPath.waypointsArray[i].cameraPitch << " " <<  tempPath.waypointsArray[i].cameraYaw <<  std::endl;
	}
	return tempPath;
	
}

turbine_segmentation::WaypointsPath topBroad(const std::vector < std::tuple<float, float, float> > & blade, const turbine_segmentation::WaypointsPath & trail, float bladeAng)
{
	float oppositeYaw = currentYawGlobal >= 0? -180 + currentYawGlobal: currentYawGlobal + 180;
	
	float angleOffset = bladeAng >= 0 && bladeAng <= 180? abs(bladeAng - 90): abs(bladeAng - 270);
	float xDisplacement = 10 * sin(angleOffset * M_PI/180);
	float yDisplacement = 10 * cos(angleOffset * M_PI/180);
	
	turbine_segmentation::WaypointsPath tempTopBroad;
	for(int i = 0; i < blade.size(); i++)
	{
		wp.latitude = (std::get<0>(blade[i]) + trail.waypointsArray[i].latitude) / 2;
		wp.longitude = (std::get<1>(blade[i]) + trail.waypointsArray[i].longitude) / 2;
		
		if (bladeAng >= 0 && bladeAng <= 180)
				wp.altitude = std::get<2>(blade[i]) + yDisplacement;
		else 
				wp.altitude = std::get<2>(blade[i]) - yDisplacement;
				
		if (bladeAng >=0 && bladeAng <=90)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal + 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				//For valid drone yaw
				float newYaw = currentYawGlobal - 90;
				if (newYaw <= -180)
					newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
					newYaw = -180 + abs(newYaw - 180);
				wp.yaw = newYaw;
				wp.cameraPitch = -bladeAng - 40;
				
				//Gimbal yaw is offset by 30 degrees
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}
		else if (bladeAng > 90 && bladeAng <= 180)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal - 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal + 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);
				wp.yaw = newYaw;
				wp.cameraPitch = - (90 - angleOffset) - 40;
				
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}
		else if (bladeAng > 180 && bladeAng <= 270)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal - 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal + 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = 90 - angleOffset - 40;
				
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;

		}
		else //bladeAng > 270 and < 360
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal + 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal - 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = 90 - angleOffset - 40;

				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}

		tempTopBroad.waypointsArray.push_back(wp);	
	}

        std::cout << "Printing the TOP path" << std::endl;
        for (int i = 0; i < tempTopBroad.waypointsArray.size(); i++)
        {
                std::cout << tempTopBroad.waypointsArray[i].latitude << " " << tempTopBroad.waypointsArray[i].longitude << " " << \
                tempTopBroad.waypointsArray[i].altitude << " " << tempTopBroad.waypointsArray[i].yaw << " " << \
                tempTopBroad.waypointsArray[i].cameraPitch << " " <<  tempTopBroad.waypointsArray[i].cameraYaw << std::endl;
        }
        
	//Allow only 80% of path to avoid danger zone near tower
	if (bladeAng >= 180 && bladeAng < 270)
	{
		int sizePath = tempTopBroad.waypointsArray.size();
		int removalIndices = floor(0.3 * sizePath);
		for (int i = 0; i < removalIndices; i++)
			tempTopBroad.waypointsArray.erase(tempTopBroad.waypointsArray.begin());		
	}

	return tempTopBroad;
}

turbine_segmentation::WaypointsPath lowBroad(const std::vector < std::tuple<float, float, float> > & blade, const turbine_segmentation::WaypointsPath & trail, float bladeAng)
{
	float oppositeYaw = currentYawGlobal >= 0? -180 + currentYawGlobal: currentYawGlobal + 180;
	
	float angleOffset = bladeAng >= 0 && bladeAng <= 180? abs(bladeAng - 90): abs(bladeAng - 270);
	float xDisplacement = 10 * sin(angleOffset * M_PI/180);
	float yDisplacement = 10 * cos(angleOffset * M_PI/180);
	
	turbine_segmentation::WaypointsPath tempLowBroad;
	for(int i = 0; i < blade.size(); i++)
	{
		wp.latitude = (std::get<0>(blade[i]) + trail.waypointsArray[i].latitude) / 2;
		wp.longitude = (std::get<1>(blade[i]) + trail.waypointsArray[i].longitude) / 2;
		
		if (bladeAng >= 0 && bladeAng <= 180)
				wp.altitude = std::get<2>(blade[i]) - yDisplacement;
		else 
				wp.altitude = std::get<2>(blade[i]) + yDisplacement;
				
		if (bladeAng >=0 && bladeAng <=90)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal - 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal + 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = bladeAng - 40;
				
				//Gimbal yaw is offset by 30 degrees
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}
		else if (bladeAng > 90 && bladeAng <= 180)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal + 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal - 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = 90 - angleOffset - 40;
				
				//Gimbal yaw is offset by 30 degrees
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}
		else if (bladeAng > 180 && bladeAng <= 270)
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal + 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal - 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = -(90 - angleOffset) - 40;
				
				//Gimbal yaw is offset by 30 degrees
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}
		else //bladeAng > 270 and < 360
		{
				std::pair<float, float> tpBrd = gpsOffset(wp.latitude, wp.longitude, xDisplacement, currentYawGlobal - 90);
				wp.latitude = tpBrd.first;
				wp.longitude = tpBrd.second;
				
				float newYaw = currentYawGlobal + 90;
				if (newYaw <= -180)
						newYaw = 180 - abs(newYaw + 180);
				else if (newYaw >= 180)
						newYaw = -180 + abs(newYaw - 180);

				wp.yaw = newYaw;
				wp.cameraPitch = -(90 - angleOffset) - 40;
				
				//Gimbal yaw is offset by 30 degrees
				float newCameraYaw = newYaw - 30;
				if (newCameraYaw < -180)
					newCameraYaw = 180 - abs(180 + newCameraYaw);
				wp.cameraYaw = newCameraYaw;
		}

		tempLowBroad.waypointsArray.push_back(wp);	
	}

        std::cout << "Printing the LOW path" << std::endl;
        for (int i = 0; i < tempLowBroad.waypointsArray.size(); i++)
        {
                std::cout << tempLowBroad.waypointsArray[i].latitude << " " << tempLowBroad.waypointsArray[i].longitude << " " << \
                tempLowBroad.waypointsArray[i].altitude << " " <<  tempLowBroad.waypointsArray[i].yaw << " " << \
                tempLowBroad.waypointsArray[i].cameraPitch << " " <<  tempLowBroad.waypointsArray[i].cameraYaw << std::endl;
        }

	//Allow only 80% of path to avoid danger zone near tower
	if (bladeAng > 90 && bladeAng < 180)
	{
		int sizePath = tempLowBroad.waypointsArray.size();
		int removalIndices = floor(0.3 * sizePath);
		for (int i = 0; i < removalIndices; i++)
			tempLowBroad.waypointsArray.erase(tempLowBroad.waypointsArray.begin());		
	}
		
	return tempLowBroad;
}


bool operator== (const turbine_segmentation::Waypoints & wp1, const turbine_segmentation::Waypoints & wp2) 
{
	return (wp1.latitude == wp2.latitude) && (wp1.longitude == wp2.longitude) && (wp1.altitude == wp2.altitude);
}

float distance(float lat1, float lon1, float alt1, float lat2, float lon2, float alt2)
{
	float R = 6378137; // meters
    float a1 = lat1 * M_PI/180; 
	float a2 = lat2 * M_PI/180;
	float b1 = (lat2-lat1) * M_PI/180;
	float b2 = (lon2-lon1) * M_PI/180;

	float a = sin(b1/2) * sin(b1/2) + cos(a1) * cos(a2) * sin(b2/2) * sin(b2/2);
	float c = 2 * atan2(sqrt(a), sqrt(1-a));
	float d = R * c;
	return sqrt(d*d + (alt1 - alt2) * (alt1 - alt2));
	
}

template <typename A, typename T>
void removeIndicesFromVector(std::vector<T> & v, std::vector<A> & rm )
{
    std::for_each(rm.crbegin(), rm.crend(), [&v](int index) { v.erase(begin(v) + index); });
}

turbine_segmentation::WaypointsPath filteredPath(turbine_segmentation::WaypointsPath & path)
{
	std::vector<int> temp;
	
	for(int i = 0; i < path.waypointsArray.size(); i++)
	{
		if (i == path.waypointsArray.size() -2)
		{
			if( abs( distance(path.waypointsArray[i+1].latitude, path.waypointsArray[i+1].longitude, path.waypointsArray[i+1].altitude,\
			 path.waypointsArray[i].latitude, path.waypointsArray[i].longitude, path.waypointsArray[i].altitude )) <= 6)
			{
				temp.push_back(i+1);
				break;
			}
		}
		else if ( abs( distance(path.waypointsArray[i+1].latitude, path.waypointsArray[i+1].longitude, path.waypointsArray[i+1].altitude,\
			 path.waypointsArray[i].latitude, path.waypointsArray[i].longitude, path.waypointsArray[i].altitude )) <= 6 && i != path.waypointsArray.size()-1)
		{
			temp.push_back(i+1);
			i += 1;
		}
	}
	
	removeIndicesFromVector(path.waypointsArray, temp);
	temp.clear();
	return path;
}

void addPath(turbine_segmentation::WaypointsPath & subPath)
{
	for (int i = 0; i < subPath.waypointsArray.size(); i++)
	{
		wpPath.waypointsArray.push_back(subPath.waypointsArray[i]);
		std::cout << subPath.waypointsArray[i].latitude << " " << subPath.waypointsArray[i].longitude << " " << subPath.waypointsArray[i].altitude << " " << subPath.waypointsArray[i].yaw << " " << subPath.waypointsArray[i].cameraPitch << " " << subPath.waypointsArray[i].cameraYaw << std::endl;	
	}
}

void createWaypointFlight(const std::vector < std::tuple<float, float, float> > & b1, const std::vector < std::tuple<float, float, float> > & b2,
						const std::vector < std::tuple<float, float, float> > & b3, float curYaw)
{
	float origYaw = currentYawGlobal;
	float oppositeYaw = currentYawGlobal >= 0? -180 + currentYawGlobal: currentYawGlobal + 180;
	
	//Each blade path consists of 2 gps coordinates.
	//Begin Waypoint path starting from the end
	//of blade 3

	wp.latitude = std::get<0>(b3[b3.size()-1]);
	wp.longitude = std::get<1>(b3[b3.size()-1]);
	wp.altitude = std::get<2>(b3[b3.size()-1]) + 6;
	wp.yaw = currentYawGlobal;
	wp.cameraPitch = -40;
	float newCameraYaw = currentYawGlobal - 30; //Gimbal yaw is offset 30 degrees
	if (newCameraYaw < -180)
		newCameraYaw = 180 - abs(180 + newCameraYaw);
	wp.cameraYaw = newCameraYaw;
	//wp.cameraYaw = currentYawGlobal;
	std::cout << "Starting Yaw " << currentYawGlobal << std::endl;	
	std::cout << "Transition 1 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	wpPath.waypointsArray.push_back(wp);
	
	//Second waypoint
	std::pair<float, float> p = gpsOffset(wp.latitude, wp.longitude, 20, currentYawGlobal);
	
	wp.latitude = p.first;
	wp.longitude = p.second;
	wp.altitude = std::get<2>(b3[b3.size()-1]) + 6;
	wp.yaw = oppositeYaw;
	wp.cameraPitch = -40;
	float newCameraYaw2 = oppositeYaw - 30; //Gimbal yaw is offset 30 degrees
	if (newCameraYaw2 < -180)
		newCameraYaw2 = 180 - abs(180 + newCameraYaw2);	
	wp.cameraYaw = newCameraYaw2;
	//wp.cameraYaw = oppositeYaw;
	
	std::cout << "Transition 2 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	wpPath.waypointsArray.push_back(wp);
	
	
	//Compose the trailing edge of blade 3 and add it to waypointPath
	//tip to hub
	turbine_segmentation::WaypointsPath b3Trailing = trailingEdge(b3);

	//hub to tip
	//std::reverse(b3Trailing.waypointsArray.begin(), b3Trailing.waypointsArray.end());
	turbine_segmentation::WaypointsPath b3TrailingFixed;
	for(int i = b3Trailing.waypointsArray.size() - 1; i >=0; i--)
	{
		b3TrailingFixed.waypointsArray.push_back(b3Trailing.waypointsArray[i]);
	}

	//hub to tip
	turbine_segmentation::WaypointsPath b3TopBroad = topBroad(b3, b3TrailingFixed, bladeThreeAngle);
	//hub to tip
	turbine_segmentation::WaypointsPath b3LowBroad = lowBroad(b3, b3TrailingFixed, bladeThreeAngle);
	
	//Reverse after building topBroad and filter < 5m
	//tip to hub
	//std::reverse(b3Trailing.waypointsArray.begin(), b3Trailing.waypointsArray.end());
	
	//tip to hub
	turbine_segmentation::WaypointsPath b3TrailingEdgeFiltered = filteredPath(b3Trailing);
	//hub to tip
	turbine_segmentation::WaypointsPath b3TopBroadFiltered = filteredPath(b3TopBroad);
	//hub to tip
	turbine_segmentation::WaypointsPath b3LowBroadFiltered = filteredPath(b3LowBroad);
	
		
	///////////////////////////////////////////////////////////////////////////////
	//tip to hub
	turbine_segmentation::WaypointsPath b1Trailing = trailingEdge(b1);
	//hub to tip
	//std::reverse(b1Trailing.waypointsArray.begin(), b1Trailing.waypointsArray.end());

	turbine_segmentation::WaypointsPath b1TrailingFixed;
	for(int i = b1Trailing.waypointsArray.size() - 1; i >=0; i--)
	{
		b1TrailingFixed.waypointsArray.push_back(b1Trailing.waypointsArray[i]);
	}

	//hub to tip
	turbine_segmentation::WaypointsPath b1TopBroad = topBroad(b1, b1TrailingFixed, bladeOneAngle);
	//hub to tip
	turbine_segmentation::WaypointsPath b1LowBroad = lowBroad(b1, b1TrailingFixed, bladeOneAngle);
	//hub to tip
	turbine_segmentation::WaypointsPath b1TrailingEdgeFiltered = filteredPath(b1TrailingFixed);
	//hub to tip
	turbine_segmentation::WaypointsPath b1TopBroadFiltered = filteredPath(b1TopBroad);
	//hub to tip
	turbine_segmentation::WaypointsPath b1LowBroadFiltered = filteredPath(b1LowBroad);
	
	
	////////////////////////////////////////////////////////////////////////////////////
	//tip to hub
	turbine_segmentation::WaypointsPath b2Trailing = trailingEdge(b2);
	//hub to tip
	//std::reverse(b2Trailing.waypointsArray.begin(), b2Trailing.waypointsArray.end());
	
	turbine_segmentation::WaypointsPath b2TrailingFixed;
	for(int i = b2Trailing.waypointsArray.size() - 1; i >=0; i--)
	{
		b2TrailingFixed.waypointsArray.push_back(b2Trailing.waypointsArray[i]);
	}

	//hub to tip
	turbine_segmentation::WaypointsPath b2TopBroad = topBroad(b2, b2TrailingFixed, bladeTwoAngle);
	//hub to tip
	turbine_segmentation::WaypointsPath b2LowBroad = lowBroad(b2, b2TrailingFixed, bladeTwoAngle);
	//hub to tip
	turbine_segmentation::WaypointsPath b2TrailingEdgeFiltered = filteredPath(b2TrailingFixed);
	//hub to tip
	turbine_segmentation::WaypointsPath b2TopBroadFiltered = filteredPath(b2TopBroad);
	//hub to tip
	turbine_segmentation::WaypointsPath b2LowBroadFiltered = filteredPath(b2LowBroad);
	
	
	
	
	//add trailing edges
	addPath(b3TrailingEdgeFiltered);
	addPath(b1TrailingEdgeFiltered);
	addPath(b2TrailingEdgeFiltered);
	
	//Start with high pressure
	std::reverse(b2TopBroadFiltered.waypointsArray.begin(), b2TopBroadFiltered.waypointsArray.end());
	addPath(b2TopBroadFiltered);
	
	wp.latitude = std::get<0>(b2[0]);
	wp.longitude = std::get<1>(b2[0]);
	wp.altitude = std::get<2>(b2[0]);
	wp.yaw = b2TopBroadFiltered.waypointsArray[b2TopBroadFiltered.waypointsArray.size() - 1].yaw; //currentYawGlobal
	wp.cameraPitch = -40;
	wp.cameraYaw = b2TopBroadFiltered.waypointsArray[b2TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 3 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	wp.latitude = std::get<0>(b1[0]);
	wp.longitude = std::get<1>(b1[0]);
	wp.altitude = std::get<2>(b1[0]);
	wp.yaw = b2TopBroadFiltered.waypointsArray[b2TopBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b2TopBroadFiltered.waypointsArray[b2TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);

	std::cout << "Transition 4 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	addPath(b1LowBroadFiltered);
	
	wp.latitude = std::get<0>(b1[b1.size() - 1]);
	wp.longitude = std::get<1>(b1[b1.size() - 1]);
	wp.altitude = std::get<2>(b1[b1.size() - 1]);
	wp.yaw = b1LowBroadFiltered.waypointsArray[b1LowBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b1LowBroadFiltered.waypointsArray[b1LowBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 5 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	std::reverse(b1TopBroadFiltered.waypointsArray.begin(), b1TopBroadFiltered.waypointsArray.end());
	addPath(b1TopBroadFiltered);
	
	wp.latitude = std::get<0>(b1[0]);
	wp.longitude = std::get<1>(b1[0]);
	wp.altitude = std::get<2>(b1[0]);
	wp.yaw = b1TopBroadFiltered.waypointsArray[b1TopBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b1TopBroadFiltered.waypointsArray[b1TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 6 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	wp.latitude = std::get<0>(b3[0]);
	wp.longitude = std::get<1>(b3[0]);
	wp.altitude = std::get<2>(b3[0]);
	wp.yaw = b1TopBroadFiltered.waypointsArray[b1TopBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b1TopBroadFiltered.waypointsArray[b1TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);

	std::cout << "Transition 7 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	addPath(b3LowBroadFiltered);
	
	wp.latitude = std::get<0>(b3[b3.size() - 1]);
	wp.longitude = std::get<1>(b3[b3.size() - 1]);
	wp.altitude = std::get<2>(b3[b3.size() - 1]);
	wp.yaw = b3LowBroadFiltered.waypointsArray[b3LowBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b3LowBroadFiltered.waypointsArray[b3LowBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 8 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	std::reverse(b3TopBroadFiltered.waypointsArray.begin(), b3TopBroadFiltered.waypointsArray.end());
	addPath(b3TopBroadFiltered);
	
	
	wp.latitude = std::get<0>(b3[0]);
	wp.longitude = std::get<1>(b3[0]);
	wp.altitude = std::get<2>(b3[0]);
	wp.yaw = b3TopBroadFiltered.waypointsArray[b3TopBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b3TopBroadFiltered.waypointsArray[b3TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 9 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	wp.latitude = std::get<0>(b2[0]);
	wp.longitude = std::get<1>(b2[0]);
	wp.altitude = std::get<2>(b2[0]);
	wp.yaw = b3TopBroadFiltered.waypointsArray[b3TopBroadFiltered.waypointsArray.size() - 1].yaw;
	wp.cameraPitch = -40;
	wp.cameraYaw = b3TopBroadFiltered.waypointsArray[b3TopBroadFiltered.waypointsArray.size() - 1].cameraYaw;
	wpPath.waypointsArray.push_back(wp);
	
	std::cout << "Transition 10 " << wp.latitude << " " << wp.longitude << " " << wp.altitude << " " \
	<< wp.yaw << " " << wp.cameraPitch << " " << wp.cameraYaw << std::endl;
	
	addPath(b2LowBroadFiltered);
	

	
	

	/////// Print the computed inspection
	std::cout << "Printing the final path" << std::endl;
	for (int i = 0; i < wpPath.waypointsArray.size(); i++)
	{
		std::cout << wpPath.waypointsArray[i].latitude << " " << wpPath.waypointsArray[i].longitude << " " << wpPath.waypointsArray[i].altitude << std::endl;
	}

	pubFullWayPoint.publish(wpPath);
}

void flight_data(const turbine_segmentation::BladePath::ConstPtr& msg)
{
	if(xPath.size() == 0)
	{
		xPath.push_back(xFlight);
		yPath.push_back(yFlight);
		zPath.push_back(zFlight);
	}
	
	xFlight += msg->bladeX;
	yFlight += msg->bladeY;
	zFlight += msg->bladeZ;
	
	xPath.push_back(xFlight);
	yPath.push_back(yFlight);
	zPath.push_back(zFlight);
	
	//myfile.open ("gpsData.txt", std::ios::out | std::ios::app);
	
	//Add the incoming drone location gps location
    //myfile << std::fixed << std::setprecision(9) << gps_position_.latitude << ", " << gps_position_.longitude << std::endl;
    std::cout << "Adding flight path point" << std::endl;
    temp.push_back(std::make_tuple(gps_position_.latitude, gps_position_.longitude, gps_position_.altitude - homeAltitude));
    
    if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 1)
    {
		std::cout << "First Blade" << std::endl;
		
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		blade1 = temp;
		std::cout << "Lenth of blade 1 " << blade1.size() << std::endl;
		
		blade1.erase(blade1.begin());
		blade1.erase(blade1.begin());
		blade1.erase(blade1.begin());
		blade1.erase(blade1.begin());	
		//blade1.erase(blade1.begin());
		//blade1.erase(blade1.begin());
	
		//auto first = blade1.cbegin() + 1; //leave first element
		//auto last = blade1.cbegin() + blade1.size() - 1; //leave last element
		//blade1.erase(first,last);
		
		temp.clear();
		bladesTraversed++;
		
		std::cout << "\n" << std::endl;
		for(int i =0; i < blade1.size(); i++)
		{
			float x = std::get<0>(blade1[i]);
			float y = std::get<1>(blade1[i]);
			float z = std::get<2>(blade1[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		
	}
	else if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 2)
    {
		std::cout << "Second blade" << std::endl;
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;		
		}
		
		blade2 = temp;
		std::cout << "Lenth of blade 2 " << blade2.size() << std::endl;
		
		blade2.erase(blade2.begin());
		blade2.erase(blade2.begin());
		//blade2.erase(blade2.begin());		
		//blade2.erase(blade2.begin());
//auto first = blade2.cbegin() + 1;
		//auto last = blade2.cbegin() + blade2.size() - 1;
		//blade2.erase(first,last);

		temp.clear();
		bladesTraversed++;
		for(int i =0; i < blade2.size(); i++)
		{
			float x = std::get<0>(blade2[i]);
			float y = std::get<1>(blade2[i]);
			float z = std::get<2>(blade2[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}

	}
	else if (msg->bladeX == 100 && msg->bladeY == 100 && msg->bladeZ == 100 && bladesTraversed == 3)
    {
		std::cout << "Third blade" << std::endl;
		blade3 = temp;
		
		for(int i =0; i < temp.size(); i++)
		{
			float x = std::get<0>(temp[i]);
			float y = std::get<1>(temp[i]);
			float z = std::get<2>(temp[i]);
			std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}
		std::cout << "Lenth of blade 3 " << blade2.size() << std::endl;
		
		blade3.erase(blade3.begin());
		blade3.erase(blade3.begin());
		//blade3.erase(blade3.begin());
		//blade3.erase(blade3.begin());
		//auto first = blade3.cbegin() + 1;
		//auto last = blade3.cbegin() + blade3.size() - 1;
		//blade3.erase(first,last);

		temp.clear();
		bladesTraversed++;

		for(int i =0; i < blade3.size(); i++)
		{
				float x = std::get<0>(blade3[i]);
				float y = std::get<1>(blade3[i]);
				float z = std::get<2>(blade3[i]);
				std::cout << std::fixed << std::setprecision(9) << x << " " << y << " " << std::endl;
		}

		
		createWaypointFlight(blade1, blade2, blade3, currentYawGlobal);
	}
   
		
	//myfile << std::fixed << std::setprecision(9) << lat0 << ", " << lon0 << "\n" << std::endl;
    
    //myfile.close();
	
}

void gpsPositionSubCallback(const sensor_msgs::NavSatFix::ConstPtr& gpsPosition)
{
  countHits++;
  gps_position_ = *gpsPosition;
  if(countHits < 25)
	homeAltitude = gps_position_.altitude;
}

void attitude_data(const geometry_msgs::QuaternionStamped::ConstPtr& attitude_msg)
{
	float x = attitude_msg->quaternion.x;
	float y = attitude_msg->quaternion.y;
	float z = attitude_msg->quaternion.z;
	float w = attitude_msg->quaternion.w;

	float q0 = w;
	float q1 = x;
	float q2 = y;
	float q3 = z;

	float q2sqr = q2 * q2;
	float t0 = -2.0 * (q2sqr + q3 * q3) + 1.0;
	float t1 = 2 * (q1 * q2 + q0 * q3);
	float t2 = -2 * (q1 * q3 - q0 * q2);
	float t3 = 2 * (q2 * q3 + q0 * q1);
	float t4 = -2 * (q1 * q1 + q2sqr) + 1;
	
	t2 = (t2 > 1.0) ? 1.0 : t2;
	t2 = (t2 < -1.0) ? -1.0 : t2;
	
	float pitchQ = asin(t2) * 180/M_PI;
	float rollQ = atan2(t3, t4) * 180/M_PI;
	float yawQ = atan2(t1, t0) * 180/M_PI;
	//std::cout << "yaw " << yawQ << " pitch " << pitchQ << " roll " << rollQ << std::endl;
	currentYawGlobal = yawQ;
}

void bladeAngles(const turbine_segmentation::BladeAnglesConstPtr& anglesData)
{
	std::cout << "received" << std::endl;
	bladeOneAngle = anglesData->bladeOneAngle;
	bladeTwoAngle = anglesData->bladeTwoAngle;
	bladeThreeAngle = anglesData->bladeThreeAngle;	
}

int main (int argc, char** argv)
{
  	// Initialize ROS
	ros::init (argc, argv, "roadmapBlade");
	ros::NodeHandle nh;

	ros::Subscriber bladeCurve = nh.subscribe("/flightPath",1000,flight_data);
	
	ros::Subscriber gpsPositionSub = nh.subscribe("dji_osdk_ros/gps_position", 1000, &gpsPositionSubCallback);
	ros::Subscriber subAttitude = nh.subscribe("/dji_osdk_ros/attitudeRPY",1,attitude_data);

	pubFullWayPoint = nh.advertise<turbine_segmentation::WaypointsPath>("/waypointPath", 1000);
	
	ros::Subscriber sub = nh.subscribe ("/turbineData", 1, bladeAngles);

	while(ros::ok())
	{
		if (VIEW_PLOT)
		{
			keywords.insert(std::pair<std::string, std::string>("label", "blade curvature") );
			//plt::clf();	
			plt::plot3(xPath,yPath,zPath,keywords);

			plt::axis("square");
			plt::xlim(-60, 0);
			plt::ylim(0, 30);
			plt::show();
		}
		ros::spinOnce();
	}
	return 0;
}
