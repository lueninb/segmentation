#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>

#include <turbine_segmentation/BladePath.h>
#include <turbine_segmentation/BladeTrackingCommand.h>

#include <math.h>
#include <vector>
#include <numeric>
#include <string>

#define PROXIMITY_ZONE 8

ros::Publisher pubTowerApproach;
double approachDistance = 0;
double currentState = -1;
double sleepForFlight = 0;
double flightCmd = 0;

bool distanceService(turbine_segmentation::BladeTrackingCommand::Request &req, turbine_segmentation::BladeTrackingCommand::Response &resp)
{
	approachDistance = req.distance;
	currentState = req.state;
	resp.response = true;
	return true;
}

void cloud_cb (ros::NodeHandle &node_handle, ros::Subscriber &subTest, const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{

		// Container for original & filtered data
		pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2; 
		// Convert to PCL data type
		pcl_conversions::toPCL(*cloud_msg, *cloud);
		
		//Calculate the distance to nearest object
		pcl::PointCloud<pcl::PointXYZ>::Ptr vertices (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromPCLPointCloud2(*cloud, *vertices);
		float minDistanceToTarget = FLT_MAX;
		float currentDist = FLT_MAX;
		float xClosest, yClosest, zClosest = FLT_MAX;
		float lastDist = FLT_MAX;
		
		float xTower, yTower, zTower = FLT_MAX;

		for (int idx = 0; idx < vertices->size(); idx++)
		{
				pcl::PointXYZ singlePoint = vertices->points[idx];
				float x = singlePoint._PointXYZ::data[0];
				float y = singlePoint._PointXYZ::data[1];
				float z = singlePoint._PointXYZ::data[2];
				
				float currentDist = sqrt(x*x + y*y + z*z);
				if (abs(z) < PROXIMITY_ZONE && currentDist != 0.0 && currentDist < minDistanceToTarget)
				{
					lastDist = minDistanceToTarget;
					minDistanceToTarget = currentDist;
					if (abs(minDistanceToTarget - lastDist) < 3 && minDistanceToTarget > 12)
					{
						xClosest = x;
						yClosest = y;
						zClosest = z;	
					}
					std::cout << "Min Dist " << minDistanceToTarget << " Last " << lastDist << std::endl;			
				}		
		}
		
		flightCmd = round(10 * xClosest)/10 - approachDistance;
		std::cout << "Approach " << flightCmd << std::endl;
		//subTest = node_handle.subscribe<sensor_msgs::PointCloud2>("/nothing", 10,
		//  boost::bind(&cloud_cb, boost::ref(node_handle), boost::ref(subTest), _1));
		  
}


int main (int argc, char** argv)
{
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
  	
  // Initialize ROS
  ros::init (argc, argv, "distance");
  ros::NodeHandle nh;


  // Create a ROS subscriber for the input point cloud
  //subLidar = nh.subscribe ("/livox/lidar", 10, cloud_cb);
  
  ros::ServiceClient client = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("currentState");
  
  ros::Subscriber subLidar = nh.subscribe<sensor_msgs::PointCloud2>("/livox/lidar", 10,
	  boost::bind(&cloud_cb, boost::ref(nh), boost::ref(subLidar), _1));
      
  // Create ROS publisher for blade path tracking the blade
  pubTowerApproach = nh.advertise<turbine_segmentation::BladePath>("bladePath", 1000);
  
  ros::ServiceServer server = nh.advertiseService("distanceService", distanceService);
  
  while(ros::ok())
  {
		ros::Duration(1.0).sleep();
		std::cout << "DIST Current State " << currentState << std::endl;
		if (currentState == 0)
		{
			//Publish initial distance command
			turbine_segmentation::BladePath distanceMessage;
			distanceMessage.bladeX = 0;
			distanceMessage.bladeY = flightCmd;
			std::cout << "message " << distanceMessage.bladeY << std::endl;
			distanceMessage.bladeZ = 0;
			pubTowerApproach.publish(distanceMessage);
			
			sleepForFlight = abs(distanceMessage.bladeY * 1.7);
			
			std::cout << "Flight sleep " << sleepForFlight << std::endl;
			ros::Duration(sleepForFlight).sleep();
			turbine_segmentation::BladeTrackingCommand srv;
			srv.request.initBladeAngle = 0;
			srv.request.distance = 0;
			srv.request.state = 1;
			if (client.call(srv))
			{
				std::cout << "Message sent " << std::endl;
				currentState = 1; //Transition to next state blade angles
			}
		}
		
		if (currentState == 2)
		{
			//Publish initial distance command
			turbine_segmentation::BladePath distanceMessage10;
			distanceMessage10.bladeX = 0;
			distanceMessage10.bladeY = flightCmd;
			std::cout << "message " << distanceMessage10.bladeY << std::endl;
			distanceMessage10.bladeZ = 0;
			pubTowerApproach.publish(distanceMessage10);
			
			sleepForFlight = abs(distanceMessage10.bladeY * 1.7);
			
			std::cout << "Flight sleep " << sleepForFlight << std::endl;
			ros::Duration(sleepForFlight).sleep();
			
			turbine_segmentation::BladeTrackingCommand srvDist10;
			srvDist10.request.initBladeAngle = 0;
			srvDist10.request.distance = 0;
			srvDist10.request.state = 3;
			if (client.call(srvDist10))
			{
				std::cout << "Message sent " << std::endl;
				currentState = 3; //Transition to next state blade tracker
			}
		}
		 // Spin
		ros::spinOnce();
	  
  }
  

}

