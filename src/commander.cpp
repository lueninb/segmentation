#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl_ros/point_cloud.h>

#include <turbine_segmentation/BladePath.h>
#include <turbine_segmentation/BladeTrackingCommand.h>
#include <turbine_segmentation/BladeAngles.h>

#include <math.h>
#include <vector>
#include <numeric>
#include <string>
#include <tuple>
#include <BladeFlight.h>

int curState = 0;
double curAngle = 0;
double curDistance = 0;
double bladeOneAngle = 0;
double bladeTwoAngle = 0;
double bladeThreeAngle = 0;

using namespace std;

vector < tuple<double, double, double> > bladeEdge;


bool currentState(turbine_segmentation::BladeTrackingCommand::Request &req, turbine_segmentation::BladeTrackingCommand::Response &resp)
{
	curAngle = req.initBladeAngle;
	curDistance = req.distance;
	curState = req.state;
	std::cout << "Received response " << std::endl;
	resp.response = true;
	return true;
}

void bladeAngles(const turbine_segmentation::BladeAnglesConstPtr& anglesData)
{
	bladeOneAngle = anglesData->bladeOneAngle;
	bladeTwoAngle = anglesData->bladeTwoAngle;
	bladeThreeAngle = anglesData->bladeThreeAngle;	
}
/*
void flightPath(const turbine_segmentation::BladePath::ConstPtr& flightMsg)
{
	double x = flightMsg->bladeX;
	double y = flightMsg->bladeY;
	double z = flightMsg->bladeZ;
	
	bladeEdge.push_back(make_tuple(x, y, z));
}*/

int main (int argc, char** argv)
{
	pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

	// Initialize ROS
	ros::init (argc, argv, "commander");
	ros::NodeHandle nh;
	
	//Subscribe to output of blade angle node
	ros::Subscriber sub = nh.subscribe ("/turbineData", 1, bladeAngles);
	
	//Subscribe to blade flight taken by drone
	//ros::Subscriber subFlight = nh.subscribe ("/flightPath", 10, flightPath);
	
	//publish the return to hub flight message
	ros::Publisher pubHubPath = nh.advertise<turbine_segmentation::BladePath>("bladePath", 1000);

	ros::ServiceServer server = nh.advertiseService("currentState", currentState);
	
	//Distance service client
	ros::ServiceClient distanceClient = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("distanceService");
	
	//Segmentation service client
	ros::ServiceClient segmentationClient = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("segmentationService");
	
	//BladeTracking service client
	ros::ServiceClient bladeTrackerClient = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("bladeTrackerService");

	while(ros::ok())
	{	
		ros::Duration(1.0).sleep();
		if (curState == 0) //Approach to 15m
		{
			//Set home position for return to hub
			std::cout << "Set home position" << std::endl;
			turbine_segmentation::BladePath setHome;
			setHome.bladeX = 0;
			setHome.bladeY = 0;
			setHome.bladeZ = 0;
			pubHubPath.publish(setHome);
			ros::Duration(2).sleep();
			
			turbine_segmentation::BladeTrackingCommand srv;
			srv.request.initBladeAngle = 0;
			srv.request.distance = 17;
			srv.request.state = 0;
			std::cout << "Initial state" << std::endl;
			if(distanceClient.call(srv))
			{
				std::cout << "Begin Approach to 15m" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 1) //Perform segmentation/blade angles
		{
			std::cout << "Approach to 15m Complete!\n" << std::endl;
			turbine_segmentation::BladeTrackingCommand srvSeg;
			srvSeg.request.initBladeAngle = 0;
			srvSeg.request.distance = 0;
			srvSeg.request.state = 1;
			if(segmentationClient.call(srvSeg))
			{
				std::cout << "Begin Segmentation & Blade Angles" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 2) //Approach to 10m
		{
			std::cout << "Segmentation Complete!" << std::endl;
			std::cout << "Angles " << bladeOneAngle << ", " << bladeTwoAngle << ", " << bladeThreeAngle << "\n" << std::endl;
			
			turbine_segmentation::BladeTrackingCommand srvDist10;
			srvDist10.request.initBladeAngle = 0;
			srvDist10.request.distance = 10;
			srvDist10.request.state = 2;
			if(distanceClient.call(srvDist10))
			{
				std::cout << "Begin Approach to 10m" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 3) //Perform blade tracking of 1st blade
		{
			std::cout << "Approach to 10m Complete!\n" << std::endl;
			//bladeEdge.clear(); // Clear initial commands of distance node
			turbine_segmentation::BladeTrackingCommand srvBlade1;
			srvBlade1.request.initBladeAngle = round(bladeOneAngle);
			srvBlade1.request.distance = 0;
			srvBlade1.request.state = 3;
			ros::Duration(12).sleep();
			if(bladeTrackerClient.call(srvBlade1))
			{
				std::cout << "Perform 1st Blade Tracking\n" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 4) //Fly back to hub
		{
			std::cout << "Returning to Hub..." << std::endl;
			double flyHubSleep = 0;
			/*
			if (curAngle == round(bladeOneAngle))  //Return to hub after tracking blade1
			{
				bladeEdge.erase(bladeEdge.begin(), bladeEdge.begin() + 2); //Remove first two distance node paths
				BladeFlight blade1{bladeOneAngle, bladeEdge};
				flyHubSleep = blade1.getHomeSleep();
			}
			else if (curAngle == round(bladeTwoAngle)) //Return to hub after tracking blade2
			{
				BladeFlight blade2{bladeTwoAngle, bladeEdge};
				flyHubSleep = blade2.getHomeSleep();
			}
			else  // Return to hub after tracking blade3
			{	
				BladeFlight blade3{bladeThreeAngle, bladeEdge};
				flyHubSleep = blade3.getHomeSleep();
			}
			*/
			std::cout << "Blade Scanned " << curAngle << std::endl;
			std::cout << "B1 " << bladeOneAngle << " B2 " << bladeTwoAngle << " B3 " << bladeThreeAngle << std::endl; 

			turbine_segmentation::BladePath goHub;
			goHub.bladeX = 100;
			goHub.bladeY = 100;
			goHub.bladeZ = 100;
			pubHubPath.publish(goHub);	
			//Ignore the BladeFlight computation and sleep for 30 seconds
			std::cout << "Wait to return home" << std::endl;
			ros::Duration(30).sleep();
			
			if (curAngle == round(bladeOneAngle))
				curState = 5; //Move onto 2nd blade
			else if (curAngle == round(bladeTwoAngle))
				curState = 6;
			else
				curState = 10;
			bladeEdge.clear();
		}
		else if (curState == 5) //Perform blade tracking of 2nd blade
		{	
			std::cout << "Reached Home!\n" << std::endl;
			
			turbine_segmentation::BladeTrackingCommand srvBlade2;
			srvBlade2.request.initBladeAngle = round(bladeTwoAngle);
			srvBlade2.request.distance = 0;
			srvBlade2.request.state = 5;
			if(bladeTrackerClient.call(srvBlade2))
			{
				std::cout << "Perform 2nd Blade Tracking\n" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 6) //Perform blade tracking of 3rd blade
		{	
			std::cout << "Reached Home\n" << std::endl;
			
			turbine_segmentation::BladeTrackingCommand srvBlade3;
			srvBlade3.request.initBladeAngle = round(bladeThreeAngle);
			srvBlade3.request.distance = 0;
			srvBlade3.request.state = 6;
			if(bladeTrackerClient.call(srvBlade3))
			{
				std::cout << "Perform 3rd Blade Tracking\n" << std::endl;
				curState = 10;
			}
		}
		else if (curState == 7)
		{
			std::cout << "Scan remaining turbine..." << std::endl;
			
			//Publish end of blade tracking to drone_master
			//which then forwards message to curvatureofblade
			turbine_segmentation::BladePath endOfBladeThree;
			endOfBladeThree.bladeX = -500;
			endOfBladeThree.bladeY = -500;
			endOfBladeThree.bladeZ = -500;
			pubHubPath.publish(endOfBladeThree);	
		}
		
		
		//else
		//	std::cout << "NO STATE" << std::endl;

		ros::spinOnce();
	}

	ROS_INFO_STREAM("Complete");

	return 0;
}
